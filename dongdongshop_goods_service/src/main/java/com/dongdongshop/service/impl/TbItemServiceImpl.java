package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbItemMapper;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.pojo.TbItemExample;
import com.dongdongshop.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbItemServiceImpl implements TbItemService {


    @Autowired
    private TbItemMapper tbItemMapper;

    //根据goodsId查询tbItem表
    @Override
    public List<TbItem> selectItemListByGoodsId(Long goodsId) {
        TbItemExample tbItemExample = new TbItemExample();
        TbItemExample.Criteria criteria = tbItemExample.createCriteria();

        //状态为正常的1
        criteria.andStatusEqualTo("1");
        //库存大于0
        criteria.andNumGreaterThan(0);
        //goodsId为传入的goodsId
        criteria.andGoodsIdEqualTo(goodsId);
        //按默认倒叙排序
        tbItemExample.setOrderByClause("is_default desc");
        return tbItemMapper.selectByExample(tbItemExample);
    }

    //根据itemId(主键id)查询item表
    @Override
    public TbItem selectItemById(Long itemId) {
        return tbItemMapper.selectByPrimaryKey(itemId);
    }

    //再根据itemId修改item表中的num的数量
    @Override
    public void updateNum(Long itemId) {
        tbItemMapper.updateNum(itemId);
    }

    @Override
    public List<TbItem> getAll(String name, Integer startIndex, Integer pageSize) {
        return null;
    }

    //根据商品id查询商品标题
    @Override
    public List<TbItem> getTitle(Long goodsId) {
        return tbItemMapper.getTitle(goodsId);
    }
}
