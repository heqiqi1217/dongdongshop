package com.dongdongshop.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.mapper.*;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.BrandService;
import com.dongdongshop.service.GoodService;
import com.dongdongshop.service.TbItemCatService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class GoodServiceImpl implements GoodService {

    @Autowired
    private TbGoodsMapper tbGoodsMapper;

    @Autowired
    private TbGoodsDescMapper tbGoodsDescMapper;

    @Autowired
    private TbItemMapper tbItemMapper;

    @Autowired
    private TbBrandMapper tbBrandMapper;

    @Autowired
    private TbItemCatMapper tbItemCatMapper;

    @Reference
    private BrandService brandService;

    @Reference
    private TbItemCatService tbItemCatService;

    //保存
    @Override
    @Transactional
    public int save(TbGoods tbGoods, TbGoodsDesc tbGoodsDesc,String listJson,String sellerName,String title,String listSpecJson) {

        //保存goods表并返回主键id
        tbGoodsMapper.save(tbGoods);

        //根据返回的主键id保存goodsdesc表
        tbGoodsDescMapper.save(tbGoods.getId(),tbGoodsDesc);

        //通过brandId去查询品牌表
        TbBrand tbBrand = brandService.selectByPrimaryKey(tbGoods.getBrandId());
        String brandName = tbBrand.getName();//品牌名,是添加在tb_item表中的

        //根据tbGoods中的category3Id作为主键,查询分类表(tbItemCat),获取name,即 tbItem表中的category
        TbItemCat tbItemCat = tbItemCatService.selectByPrimaryKey(tbGoods.getCategory3Id());
        String category = tbItemCat.getName();//分类名,是添加在tb_item表中的

        //创建时间
        Date createTime  = new Date();

        //更新时间
        Date updateTime =new Date();

        //根据返回的主键id保存tbItem表
        int i = 0;
        List<TbItem> tbItems = JSONObject.parseArray(listJson, TbItem.class);
        List<String> listSpec = JSONObject.parseArray(listSpecJson, String.class);
        for (int j = 0; j < tbItems.size(); j++) {
            //批量增加tbItem表
           i = tbItemMapper.addTbItem(tbGoods.getId(),tbItems.get(j).getPrice(),tbItems.get(j).getNum(),brandName,
                   category,sellerName,tbGoods.getSellerId(),tbGoods.getCategory3Id(),tbGoods.getCaption(),
                   title,createTime,updateTime,listSpec.get(j));
        }
        return i;
    }

    //展示商品(加分页)
    @Override
    public Page<List<TbGoods>> listGoods(String auditStatus,String goodsName,Integer pageNum,Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<TbGoods> tbGoodsList = tbGoodsMapper.listGoods(auditStatus,goodsName);
        PageInfo pageInfo = new PageInfo(tbGoodsList);

        Page<List<TbGoods>> page = new Page<List<TbGoods>>();
        page.setPageNum(pageNum);
        page.setPageSize(pageSize);
        page.setTotal(pageInfo.getTotal());
        page.setPages(pageInfo.getPages());
        page.setRows(pageInfo.getList());
        return page;
    }

    //展示详情
    @Override
    public TbGoodsDesc listGoodsDesc(Integer id) {
        return tbGoodsDescMapper.selectByPrimaryKey(id.longValue());
    }

    //批量修改审核状态
    @Override
    @Transactional
    public int updateStatus(Long[] ids, Integer auditStatus) {
        TbGoods tbGoods = new TbGoods();
        int i = 0;
        for (Long id:ids) {
            if(auditStatus == 1){  //审核通过
                tbGoods.setAuditStatus("1");
                tbGoodsMapper.updateStatus(id,tbGoods);
            }else if(auditStatus == 2){  //驳回
                tbGoods.setAuditStatus("2");
                tbGoodsMapper.updateStatus(id,tbGoods);
            }
        }
        return i;
    }

    //批量删除
    @Override
    @Transactional
    public int deleteGoodsByIds(Long[] ids) {
        int i = 0;
        for (Long id:ids) {
            //删除goods表
            tbGoodsMapper.deleteByPrimaryKey(id);

            //删除goodsDesc表
            tbGoodsDescMapper.deleteGoodsDesc(id);

            //删除tb_item表
            i = tbItemMapper.deleteItem(id);
        }
        return i;
    }

    //查询商品表goods
    @Override
    public List<TbGoods> selectGoods() {
        return tbGoodsMapper.selectByExample(null);
    }

    //根据goodsId查询goods表
    @Override
    public TbGoods selectGoodsByGoodsId(Long goodsId) {
        return tbGoodsMapper.selectByPrimaryKey(goodsId);
    }
}
