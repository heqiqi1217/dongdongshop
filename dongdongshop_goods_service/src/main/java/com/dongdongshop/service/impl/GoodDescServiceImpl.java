package com.dongdongshop.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dongdongshop.mapper.TbGoodsDescMapper;
import com.dongdongshop.pojo.TbGoodsDesc;
import com.dongdongshop.service.GoodDescService;
import org.springframework.beans.factory.annotation.Autowired;


@Service
@org.springframework.stereotype.Service
public class GoodDescServiceImpl implements GoodDescService {

    @Autowired
    private TbGoodsDescMapper tbGoodsDescMapper;

    @Override
    public TbGoodsDesc selectGoodsListByGoodsId(Long goodsId) {
        return tbGoodsDescMapper.selectByPrimaryKey(goodsId);
    }
}
