package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbGoodsDesc;
import com.dongdongshop.pojo.TbGoodsDescExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbGoodsDescMapper {
    int countByExample(TbGoodsDescExample example);

    int deleteByExample(TbGoodsDescExample example);

    int deleteByPrimaryKey(Long goodsId);

    int insert(TbGoodsDesc record);

    int insertSelective(TbGoodsDesc record);

    List<TbGoodsDesc> selectByExample(TbGoodsDescExample example);

    TbGoodsDesc selectByPrimaryKey(Long goodsId);

    int updateByExampleSelective(@Param("record") TbGoodsDesc record, @Param("example") TbGoodsDescExample example);

    int updateByExample(@Param("record") TbGoodsDesc record, @Param("example") TbGoodsDescExample example);

    int updateByPrimaryKeySelective(TbGoodsDesc record);

    int updateByPrimaryKey(TbGoodsDesc record);

    //保存goodsdesc表
    int save(@Param("id") Long id, @Param("tbGoodsDesc")TbGoodsDesc tbGoodsDesc);

    //删除goodsDesc表
    int deleteGoodsDesc(long id);
}