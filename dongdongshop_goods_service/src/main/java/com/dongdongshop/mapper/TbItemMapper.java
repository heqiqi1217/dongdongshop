package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.pojo.TbItemExample;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbItemMapper {
    int countByExample(TbItemExample example);

    int deleteByExample(TbItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbItem record);

    int insertSelective(TbItem record);

    List<TbItem> selectByExample(TbItemExample example);

    TbItem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbItem record, @Param("example") TbItemExample example);

    int updateByExample(@Param("record") TbItem record, @Param("example") TbItemExample example);

    int updateByPrimaryKeySelective(TbItem record);

    int updateByPrimaryKey(TbItem record);

    //批量增加tbItem表
    int addTbItem(@Param("id")Long id, @Param("price") BigDecimal price, @Param("num") Integer num,
                  @Param("brandName")String brandName, @Param("category")String category,
                  @Param("sellerName")String sellerName,@Param("sellerId")String sellerId,
                  @Param("category3Id")Long category3Id,@Param("caption")String caption,@Param("title")String title,
                  @Param("createTime")Date createTime, @Param("updateTime")Date updateTime, @Param("spec")String spec);


    //批量删除tbItem表
    int deleteItem(Long id);

    //再根据itemId修改item表中的num的数量
    void updateNum(Long itemId);

    //根据商品id查询商品标题
    List<TbItem> getTitle(Long goodsId);
}