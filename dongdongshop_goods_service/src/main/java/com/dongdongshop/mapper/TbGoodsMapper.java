package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbGoodsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbGoodsMapper {
    int countByExample(TbGoodsExample example);

    int deleteByExample(TbGoodsExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbGoods record);

    int insertSelective(TbGoods record);

    List<TbGoods> selectByExample(TbGoodsExample example);

    TbGoods selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbGoods record, @Param("example") TbGoodsExample example);

    int updateByExample(@Param("record") TbGoods record, @Param("example") TbGoodsExample example);

    int updateByPrimaryKeySelective(TbGoods record);

    int updateByPrimaryKey(TbGoods record);

    //保存goods表并返回主键id
    void save(TbGoods tbGoods);

    //展示商品(加模糊查询)
    List<TbGoods> listGoods(@Param("auditStatus")String auditStatus,@Param("goodsName") String goodsName);

    //批量修改审核状态
    int updateStatus(@Param("id") Long id, @Param("tbGoods") TbGoods tbGoods);
}