package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbAddressMapper;
import com.dongdongshop.pojo.TbAddress;
import com.dongdongshop.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private TbAddressMapper tbAddressMapper;

    //增加地址表
    @Override
    public int addAddress(TbAddress tbAddress) {
        return tbAddressMapper.insertSelective(tbAddress);
    }

    //展示地址
    @Override
    public List<TbAddress> getData(String userName) {
        return tbAddressMapper.getData(userName);
    }

    //回显地址
    @Override
    public TbAddress getDataById(Long id) {
        return tbAddressMapper.getDataById(id);
    }

    //修改地址
    @Override
    public int updateAddress(TbAddress tbAddress) {
        return tbAddressMapper.updateByPrimaryKeySelective(tbAddress);
    }

    //删除地址
    @Override
    public int deleteById(Long id) {
        return tbAddressMapper.deleteByPrimaryKey(id);
    }

    //默认地址
    @Override
    @Transactional
    public int changeDefault(Long id,String username) {
        tbAddressMapper.changeDefault(username);
        int i = tbAddressMapper.changeDefault1(id);
        return i;
    }
}
