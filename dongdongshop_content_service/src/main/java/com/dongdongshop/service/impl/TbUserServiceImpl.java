package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbUserMapper;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.pojo.TbUserExample;
import com.dongdongshop.service.TbUserService;
import com.dongdongshop.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbUserServiceImpl implements TbUserService {

    @Autowired
    private TbUserMapper tbUserMapper;

    //登录
    @Override
    public TbUser login(String username) {
        return tbUserMapper.login(username);
    }

    //查询出用户表中的所有用户
    @Override
    public List<TbUser> selectAllUser() {
        return tbUserMapper.selectByExample(null);
    }

    //注册
    @Override
    public void register(TbUser tbUser) {
        tbUserMapper.insertSelective(tbUser);
    }

    //根据用户名查询tb_user表获取到邮箱
    @Override
    public TbUser findPassword(String username) {
        return tbUserMapper.findPassword(username);
    }

    //根据username去修改tb_user表中的密码
    @Override
    public String updatePassword(TbUser tbUser) {
        TbUser user = findPassword(tbUser.getUsername());
        String salt = "123";
        String pwd = "999";
        String md5 = ShiroUtils.encryptPassword("MD5", pwd, salt, 3);
        user.setSalt(salt);
        user.setPassword(md5);
        tbUserMapper.updateByPrimaryKeySelective(user);
        return pwd;
    }

    @Override
    public List<TbUser> selectUserByDate() {
        return tbUserMapper.selectUserByDate();
    }
}
