package com.dongdongshop.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.mapper.TbOrderMapper;
import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.service.TbOrderService;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbOrderServiceImpl implements TbOrderService {

    @Autowired
    private TbOrderMapper tbOrderMapper;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    //增加order表
    @Override
    public void submit(TbOrder tbOrder) {
        tbOrderMapper.insertSelective(tbOrder);
    }

    //发送消息给本地事务
    @Override
    @Transactional
    public void sendStatus(String out_trade_no,String status,String trade_no) {
        TbOrder tbOrder = new TbOrder();
        tbOrder.setOutTradeNo(out_trade_no);
        tbOrder.setStatus(status);
        tbOrder.setTradeNo(trade_no);

        String str = JSONObject.toJSONString(tbOrder);

        System.out.println(str);
        //发送消息给本地事务
        Message message = MessageBuilder.withPayload(str).build();
        TransactionSendResult result = rocketMQTemplate.sendMessageInTransaction("tx-group", "tx-topic", message, out_trade_no);

    }

    //根据登录用户查询order表
    @Override
    public List<TbOrder> getOrder(String userName) {
        String status = "2";
        return tbOrderMapper.getOrder(userName,status);
    }

    //修改订单状态为已支付
    @Override
    public void updateStatus(String outTradeNo, String status, String tradeNo) {
        tbOrderMapper.updateStatus(outTradeNo,status,tradeNo);
    }

    //查询order表拿到登录用户
    @Override
    public TbOrder selectUserIdByOutTradeNo(String outTradeNo) {
        return tbOrderMapper.selectUserIdByOutTradeNo(outTradeNo);
    }

    //修改订单状态为已评价
    @Override
    public void updateStatusByOrderId(Long orderId, String status) {
        tbOrderMapper.updateStatusByOrderId(orderId,status);
    }
}
