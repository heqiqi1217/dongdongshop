package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbCitiesMapper;
import com.dongdongshop.pojo.TbCities;
import com.dongdongshop.service.TbCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbCityServiceImpl implements TbCityService {

    @Autowired
    private TbCitiesMapper tbCitiesMapper;

    //查询市
    @Override
    public List<TbCities> findCities() {
        return tbCitiesMapper.selectByExample(null);
    }
}
