package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbOrderItemMapper;
import com.dongdongshop.pojo.TbOrderItem;
import com.dongdongshop.service.TbOrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbOrderItemServiceImpl implements TbOrderItemService {

    @Autowired
    private TbOrderItemMapper tbOrderItemMapper;

    //增加orderItem表
    @Override
    public void submitOrderItem(TbOrderItem tbOrderItem) {
        tbOrderItemMapper.insertSelective(tbOrderItem);
    }

    //添加支付宝流水号到orderItem表
    @Override
    public void update(String out_trade_no, String trade_no) {
        tbOrderItemMapper.update(out_trade_no,trade_no);
    }

    //根据orderId查询orderitem表
    @Override
    public List<TbOrderItem> getOrderItem(Long orderId) {
        return tbOrderItemMapper.getOrderItem(orderId);
    }

    //从orderItem表中删除退款的商品
    @Override
    public void delete(String id) {
        tbOrderItemMapper.deleteByPrimaryKey(Long.parseLong(id));
    }

    //根据outTradeNo查询orderItem表,拿到itemId
    @Override
    public TbOrderItem selectOrderItem(String outTradeNo) {
       return tbOrderItemMapper.selectOrderItem(outTradeNo);
    }

    //根据itemId查询tb_order_item表,获取到id
    @Override
    public List<TbOrderItem> selectOrderItemGetId(Long id) {
        return tbOrderItemMapper.selectOrderItemGetId(id);
    }
}
