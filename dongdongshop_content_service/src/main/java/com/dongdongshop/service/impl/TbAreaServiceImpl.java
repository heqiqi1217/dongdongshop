package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbAreasMapper;
import com.dongdongshop.pojo.TbAreas;
import com.dongdongshop.service.TbAreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbAreaServiceImpl implements TbAreasService {

    @Autowired
    private TbAreasMapper tbAreasMapper;

    //查询区
    @Override
    public List<TbAreas> findAreas() {
        return tbAreasMapper.selectByExample(null);
    }
}
