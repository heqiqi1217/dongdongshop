package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbProvincesMapper;
import com.dongdongshop.pojo.TbCities;
import com.dongdongshop.pojo.TbProvinces;
import com.dongdongshop.service.TbProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbProvinceServiceImpl implements TbProvinceService {

    @Autowired
    private TbProvincesMapper tbProvincesMapper;

    //查询省
    @Override
    public List<TbProvinces> findProvince() {
        return tbProvincesMapper.selectByExample(null);
    }

}
