package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbOrderMapper {
    int countByExample(TbOrderExample example);

    int deleteByExample(TbOrderExample example);

    int deleteByPrimaryKey(Long orderId);

    int insert(TbOrder record);

    int insertSelective(TbOrder record);

    List<TbOrder> selectByExample(TbOrderExample example);

    TbOrder selectByPrimaryKey(Long orderId);

    int updateByExampleSelective(@Param("record") TbOrder record, @Param("example") TbOrderExample example);

    int updateByExample(@Param("record") TbOrder record, @Param("example") TbOrderExample example);

    int updateByPrimaryKeySelective(TbOrder record);

    int updateByPrimaryKey(TbOrder record);

    //修改订单状态为已支付
    void updateStatus(@Param("outTradeNo")String outTradeNo, @Param("status")String status, @Param("tradeNo")String tradeNo);

    //根据登录用户查询order表
    List<TbOrder> getOrder(@Param("userName") String userName,@Param("status")String status);

    //查询order表拿到登录用户
    TbOrder selectUserIdByOutTradeNo(String outTradeNo);

    //修改订单状态为已评价
    void updateStatusByOrderId(@Param("orderId") Long orderId,@Param("status")String status);
}