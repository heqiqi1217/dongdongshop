package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbOrderItem;
import com.dongdongshop.pojo.TbOrderItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbOrderItemMapper {
    int countByExample(TbOrderItemExample example);

    int deleteByExample(TbOrderItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbOrderItem record);

    int insertSelective(TbOrderItem record);

    List<TbOrderItem> selectByExample(TbOrderItemExample example);

    TbOrderItem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbOrderItem record, @Param("example") TbOrderItemExample example);

    int updateByExample(@Param("record") TbOrderItem record, @Param("example") TbOrderItemExample example);

    int updateByPrimaryKeySelective(TbOrderItem record);

    int updateByPrimaryKey(TbOrderItem record);

    //添加支付宝流水号到orderItem表
    void update(@Param("out_trade_no")String out_trade_no, @Param("trade_no") String trade_no);

    //根据orderId查询orderitem表
    List<TbOrderItem> getOrderItem(Long orderId);

    //根据outTradeNo查询orderItem表,拿到itemId
    TbOrderItem selectOrderItem(String outTradeNo);

    //根据itemId查询tb_order_item表,获取到id
    List<TbOrderItem> selectOrderItemGetId(Long id);
}