package com.dongdongshop.mq;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbOrderItem;
import com.dongdongshop.service.TbItemService;
import com.dongdongshop.service.TbOrderItemService;
import com.dongdongshop.service.TbOrderService;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.Message;

@RocketMQTransactionListener(txProducerGroup = "tx-group")
public class MQConsumerTransactionListener implements RocketMQLocalTransactionListener {

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbOrderService tbOrderService;

    @Reference
    private TbOrderItemService tbOrderItemService;

    @Reference
    private TbItemService tbItemService;

    /**
     * 1、MQ服务，成功接到事物消息后，执行本方法；
     * 2、处理本地事物，并将本地事物处理结果返回给MQ服务
     *  执行本地事务的方法
     *  Object:就是我们要执行本地事务的参数
     *  message:就是消费者要消费的信息
     * @return COMMIT, ROLLBACK, UNKNOWN;
     */
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        //将本地事物执行状态return到MQ服务，并且保存到redis备份（网络延迟，MQ服务产生回调的时候 可以从redis中获取）
        try {
            //消息内容
            System.out.println("消息内容-------------");
            System.out.println(new String((byte[]) message.getPayload()));

            System.out.println("本地事务需要的参数-------------");
            System.out.println(o.toString());

            TbOrder tbOrder = JSONObject.parseObject(new String((byte[]) message.getPayload()), TbOrder.class);
            String outTradeNo = tbOrder.getOutTradeNo();
            String status = tbOrder.getStatus();
            String tradeNo = tbOrder.getTradeNo();

            //修改订单状态,并添加订单流水号
            tbOrderService.updateStatus(outTradeNo,status,tradeNo);

            //根据outTradeNo查询orderItem表,拿到itemId;
            TbOrderItem tbOrderItem = tbOrderItemService.selectOrderItem(outTradeNo);

            //再根据itemId修改item表中的num的数量
            tbItemService.updateNum(tbOrderItem.getItemId());

            //int i = 9 / 0;
        } catch (Exception e) {
            //处理异常返回ROLLBACK
            redisTemplate.opsForValue().set(message.getHeaders().get("TRANSACTION_ID"), "2");
            return RocketMQLocalTransactionState.ROLLBACK;
        }
        //处理成功返回COMMIT
        redisTemplate.opsForValue().set(message.getHeaders().get("TRANSACTION_ID"), "1");
        return RocketMQLocalTransactionState.COMMIT;
    }

    /**
     * MQ服务 由于网络等原因 未收到 本地事物处理结果，回查本地事物处理结果
     */
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        //通过事务id获取 redis中 对应的本地事务执行状态
        String status = (String) redisTemplate.opsForValue().
                get(message.getHeaders().get("TRANSACTION_ID").toString());
        if ("1".equals(status)) {
            return RocketMQLocalTransactionState.COMMIT;
        } else if ("2".equals(status)) {
            //继续处理，或者直接回滚
            return RocketMQLocalTransactionState.ROLLBACK;
        } else {
            return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}
