package com.dongdongshop.aop;

import com.alibaba.fastjson.JSONObject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/*
* 1.定义切点表达式
* 2.定义通知:前置通知,后置通知,返回后通知,环绕通知,错误通知
* */
@Component
@Aspect
//切面类
public class LogAspect {

    public Logger logger = LoggerFactory.getLogger(LogAspect.class);

    //1.定义切点表达式
    @Pointcut("execution(public * com.dongdongshop.service.*.*(..)) || @annotation(com.dongdongshop.annotation.Log)")
    public void aopLog(){

    }

    //2.通知
    //前置通知
    @Before("aopLog()")
    public void before(JoinPoint joinPoint){
        //获取正在执行的方法
        Signature signature = joinPoint.getSignature();
        //获取每个被调用的service方法的参数
        Object[] args = joinPoint.getArgs();
        logger.info("正在执行的方法是:{},参数是:{}",signature,args);
    }

    //返回后通知
    @AfterReturning(pointcut = "aopLog()",returning = "obj")
    public void after(Object obj) throws Throwable{
        logger.info("方法执行结束,返回值为:{}", JSONObject.toJSONString(obj));
    }
}
