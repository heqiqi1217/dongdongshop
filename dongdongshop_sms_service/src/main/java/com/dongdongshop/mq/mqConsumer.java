package com.dongdongshop.mq;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@RocketMQMessageListener(topic="phone",consumerGroup = "myPhone-group")
public class mqConsumer implements RocketMQListener<String> {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void onMessage(String phone) {
        //取出手机号调用阿里大于发送验证码
        //连接阿里云
        DefaultProfile profile = DefaultProfile.getProfile("cn-beijing",
                "LTAI4GCbdwnjCGwQj9Z3tr3N", "27ih4pc1W1tqey9X4mmCy1JUotAX7L");
        /** use STS Token
         DefaultProfile profile = DefaultProfile.getProfile(
         "<your-region-id>",           // The region ID
         "<your-access-key-id>",       // The AccessKey ID of the RAM account
         "<your-access-key-secret>",   // The AccessKey Secret of the RAM account
         "<your-sts-token>");          // STS Token
         **/
        IAcsClient client = new DefaultAcsClient(profile);

        //发送请求
        CommonRequest request = new CommonRequest();
        //请求方式post
        request.setSysMethod(MethodType.POST);
        //域名
        request.setSysDomain("dysmsapi.aliyuncs.com");
        //版本
        request.setSysVersion("2017-05-25");
        //发送短信功能
        request.setSysAction("SendSms");
        //接收方手机号
        request.putQueryParameter("PhoneNumbers",phone);
        //签名
        request.putQueryParameter("SignName","东科创想");
        //短信模板
        request.putQueryParameter("TemplateCode","SMS_162522027");

        //生成随机验证码 6位数的
        String numeric = RandomStringUtils.randomNumeric(6);
        request.putQueryParameter("TemplateParam","{\"sms_code\":\""+ numeric +"\"}");

        //把随机生成的验证码放到redis中  失效时间30分钟
        redisTemplate.opsForValue().set(phone,numeric,30, TimeUnit.MINUTES);
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }

    }
}
