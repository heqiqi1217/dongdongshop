package com.dongdongshop.test;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

public class SendMessage {

    public static void main(String[] args) {
        //连接阿里云
        DefaultProfile profile = DefaultProfile.getProfile("cn-beijing",
                "LTAI4GCbdwnjCGwQj9Z3tr3N", "27ih4pc1W1tqey9X4mmCy1JUotAX7L");
        /** use STS Token
         DefaultProfile profile = DefaultProfile.getProfile(
         "<your-region-id>",           // The region ID
         "<your-access-key-id>",       // The AccessKey ID of the RAM account
         "<your-access-key-secret>",   // The AccessKey Secret of the RAM account
         "<your-sts-token>");          // STS Token
         **/
        IAcsClient client = new DefaultAcsClient(profile);

        //发送请求
        CommonRequest request = new CommonRequest();
        //请求方式post
        request.setSysMethod(MethodType.POST);
        //域名
        request.setSysDomain("dysmsapi.aliyuncs.com");
        //版本
        request.setSysVersion("2017-05-25");
        //发送短信功能
        request.setSysAction("SendSms");
        //接收方手机号
        request.putQueryParameter("PhoneNumbers","15737326301");
        //签名
        request.putQueryParameter("SignName","东科创想");
        //短信模板
        request.putQueryParameter("TemplateCode","SMS_162522027");
        request.putQueryParameter("TemplateParam","{\"sms_code\":\"8888\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
