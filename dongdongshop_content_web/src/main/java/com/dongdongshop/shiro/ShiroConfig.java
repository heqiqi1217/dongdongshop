package com.dongdongshop.shiro;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {
    //将shiro的过滤器链放入到Spring容器中
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(@Autowired DefaultWebSecurityManager defaultWebSecurityManager){
        //创建过滤器链工厂类
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        //添加shiro内置的过滤器链
        /*
         * 常用的过滤器链
         * anon:不需要登录就可以访问
         * authc:必须登录才能访问
         * perms:登录之后还要有权限才能访问
         * role:登录之后要有角色可以访问
         * logout:退出登录
         * */
        Map<String,String> map = new LinkedHashMap<>();
        //key是需要过滤的url,value为设置的url对应的过滤器

        //我的东东商城
        map.put("/indexController/toHomeSettingAddress","authc");

        //登录以后,才能跳转到结算页面
        map.put("/cartController/toGetOrderInfo","authc");

        //登录
        map.put("/**","anon");

        //退出登录
        map.put("/logout","logout");

        //设置安全管理器 (给shiroFilterFactoryBean注入安全管理器)
        shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager);
        shiroFilterFactoryBean.setFilterChainDefinitionMap(map);

        //跳转到登录页面  因为shiro默认登录页面为.jsp,springboot不支持jsp页面,所以需要修改跳转登录页面
        shiroFilterFactoryBean.setLoginUrl("/loginController/toLogin");
        return shiroFilterFactoryBean;
    }

    //将安全管理器放入到Spring容器中
    @Bean
    public DefaultWebSecurityManager defaultWebSecurityManager(@Autowired Realm realm){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        //关联自定义的realm类
        defaultWebSecurityManager.setRealm(realm);
        return defaultWebSecurityManager;
    }

    //把realm类放到Spring中
    @Bean
    public Realm realm(@Autowired HashedCredentialsMatcher hashedCredentialsMatcher){
        Realm realm = new Realm();
        //将加密方式关联自定义的realm类
        realm.setCredentialsMatcher(hashedCredentialsMatcher);
        return realm;
    }

    //设置密码加密
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher(){
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher(); //获取对象
        hashedCredentialsMatcher.setHashAlgorithmName("MD5");  //指定加密方式
        hashedCredentialsMatcher.setHashIterations(3);   //设置加密的次数
        hashedCredentialsMatcher.setStoredCredentialsHexEncoded(true);  //使用设置编码
        return hashedCredentialsMatcher;
    }

    //开启shiro的方言
    @Bean
    public ShiroDialect shiroDialect(){
        return new ShiroDialect();
    }
}
