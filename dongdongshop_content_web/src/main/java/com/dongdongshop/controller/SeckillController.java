package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbSeckillGoods;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.TbSeckillGoodService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("seckillController")
public class SeckillController {

    @Reference
    private TbSeckillGoodService tbSeckillGoodService;

    //跳转到seckill-index页面
    @RequestMapping("toSeckillIndex")
    public String toSeckillIndex(Model model){
        //查询tb_seckill_goods表
        List<TbSeckillGoods> tbSeckillGoodsList = tbSeckillGoodService.findGoods();
        model.addAttribute("tbSeckillGoodsList",tbSeckillGoodsList);
        return "admin/seckill-index";
    }

    //跳转到seckill-item页面,抢购页面
    @RequestMapping("findGoodsById/{id}")
    public String findGoodsById(@PathVariable("id")Long id,Model model){
        TbSeckillGoods tbSeckillGoods = tbSeckillGoodService.findGoodsById(id);
        if(tbSeckillGoods == null){  //卖完了
            return "admin/saleOVer";
        }
        model.addAttribute("tbSeckillGoods",tbSeckillGoods);
        return "admin/seckill-item";
    }

    //立即抢购
    @RequestMapping("createOrder")
    @ResponseBody
    public Result createOrder(Long goodsId){
        //先判断有没有登录
        TbUser tbUser  = (TbUser)SecurityUtils.getSubject().getPrincipal();
        if(tbUser == null){
            return Result.bulid(ConstantEnum.NO_AUTH).setResult("请先登录");
        }
        //创建订单
        try {
            tbSeckillGoodService.createOrder(goodsId,tbUser.getUsername());
        } catch (Exception e) {
            return Result.bulid(ConstantEnum.ERROR).setResult("商品已售罄");
        }
        return Result.bulid(ConstantEnum.SUCCESS);
    }

    //查询订单号
    @RequestMapping("getOrderId")
    @ResponseBody
    public Result getOrderId(){
        TbUser tbUser  = (TbUser)SecurityUtils.getSubject().getPrincipal();
        Long seckillOrderId = tbSeckillGoodService.getOrderId(tbUser.getUsername());
        if(seckillOrderId == null){
            return Result.bulid(ConstantEnum.ERROR);
        }
        return Result.bulid(ConstantEnum.SUCCESS).setResult(seckillOrderId);
    }
}
