package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.dongdongshop.config.AlipayConfig;
import com.dongdongshop.service.TbOrderItemService;
import com.dongdongshop.service.TbOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
@RequestMapping("callBack")
public class AlipayCallBack {

    /* *同步回调
     * 功能：支付宝服务器同步通知页面
     * 日期：2017-03-30
     * 说明：
     * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
     * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

     *************************页面功能说明*************************
     * 该页面仅做页面展示，业务逻辑处理请勿在该页面执行
     * out_trade_no 商户订单号
     * trade_no     支付宝交易号
     * total_amount 付款金额
     */
    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbOrderService tbOrderService;

    @Reference
    private TbOrderItemService tbOrderItemService;

    @RequestMapping("returnUrl")
    public String returnUrl(HttpServletRequest request, String out_trade_no, String trade_no, String total_amount, Model model) throws AlipayApiException {
        //获取支付宝GET过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        //验证数字签名(验签)
        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key,
                AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——
        //若验签成功,说明数字签名没有被篡改过,若失败,两个原因:1.被篡改 2.编码问题或数据丢失
        System.out.println("进入同步对调方法");
        if(signVerified) {
            System.out.println("同步回调验签成功");
            model.addAttribute("out_trade_no",out_trade_no);
            model.addAttribute("trade_no",trade_no);
            model.addAttribute("total_amount",total_amount);
            return "admin/paysuccess";
        }else {
            System.out.println("同步回调验签失败");
            return "admin/payfail";
        }
        //——请在这里编写您的程序（以上代码仅作参考）——
    }

    /**
     *异步回调
     * @param request
     * @param out_trade_no  商户订单号
     * @param trade_no       支付宝流水号
     * @param trade_status    支付状态
     * @return
     * @throws AlipayApiException
     */
    @RequestMapping("notifyUrl")
    @ResponseBody
    public String notifyUrl(HttpServletRequest request, String out_trade_no, String trade_no, String trade_status) throws AlipayApiException {
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——

	/* 实际验证过程建议商户务必添加以下校验：
	1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
	2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
	3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
	4、验证app_id是否为该商户本身。
	*/
        if(signVerified) {//验证成功
            System.out.println("异步回调验签成功");

            //用户跳转到支付页面后,取消支付,我们就可以调用支付接口进行支付
            if(trade_status.equals("TRADE_FINISHED")){
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
            }else if (trade_status.equals("TRADE_SUCCESS")){
                //支付成功和退款成功都会到这个方法
                //判断该笔订单是否在商户网站中已经做过处理(解决幂等性问题)  借助于redis来解决
                //在退款时,gmt_refund字段是交易时没有的
                String[] gmt_refunds = requestParams.get("gmt_refund");
                if(gmt_refunds != null){  //说明这个请求是退款成功的请求
                    System.out.println("进入退款请求");
                    if(!redisTemplate.boundHashOps("alipay").hasKey(trade_no + "gmt_refunds")){
                        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                        //修改订单状态为已退款,从orderItem表中删除退款的商品
                        //String status1 = "8";
                        String[] out_biz_nos = requestParams.get("out_biz_no");
                        String id = out_biz_nos[0];
                        tbOrderItemService.delete(id);
                        redisTemplate.boundHashOps("alipay").put("trade_no",trade_no + "gmt_refunds");
                    }
                }else if(!redisTemplate.boundHashOps("alipay").hasKey(trade_no + "trade")){
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //修改订单状态为已支付,并添加支付宝流水号到order表
                    String status = "2";
                    tbOrderService.sendStatus(out_trade_no,status,trade_no);


                    //添加支付宝流水号到orderItem表
                    tbOrderItemService.update(out_trade_no,trade_no);

                    redisTemplate.boundHashOps("alipay").put("trade_no",trade_no + "trade");
                }
                //如果有做过处理，不执行商户的业务程序
                //注意：
                //付款完成后，支付宝系统发送该交易状态通知
            }
            System.out.println("支付成功" + trade_no + "----" + out_trade_no);
            return "success";
        }else {//验证失败
            System.out.println("异步回调验签失败");
            //调试用，写文本函数记录程序运行情况是否正常
            //String sWord = AlipaySignature.getSignCheckContentV1(params);
            //AlipayConfig.logResult(sWord);
            return "fail";
        }
    }
}
