package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.service.TbItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("searchController")
public class SearchController {

    @Reference
    private TbItemService itemService;

    //转到search页面
    @RequestMapping("toSearch")
    public String toSearch(String name, Integer startIndex, Model model){
        model.addAttribute("name",name);
        model.addAttribute("startIndex",startIndex);
        return "admin/search";
    }

    //搜索
    @RequestMapping("getAll")
    @ResponseBody
    public Result getAll(String name, @RequestParam(defaultValue = "0")Integer startIndex, @RequestParam(defaultValue = "10") Integer pageSize){
        List<TbItem> list = itemService.getAll(name, startIndex,pageSize);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }
}
