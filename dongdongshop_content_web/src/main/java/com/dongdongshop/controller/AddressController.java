package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.AddressService;
import com.dongdongshop.service.TbAreasService;
import com.dongdongshop.service.TbCityService;
import com.dongdongshop.service.TbProvinceService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("addressController")
public class AddressController {

    @Reference
    private TbProvinceService tbProvinceService;

    @Reference
    private TbCityService tbCityService;

    @Reference
    private TbAreasService tbAreasService;

    @Reference
    private AddressService addressService;

    //展示地址
    @RequestMapping("getData")
    @ResponseBody
    public Result getData(){
        //获取登录的用户
        TbUser tbUser = (TbUser) SecurityUtils.getSubject().getPrincipal();
        List<TbAddress> tbAddresses = addressService.getData(tbUser.getUsername());
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbAddresses);
    }

    //回显地址
    @RequestMapping("getDataById")
    @ResponseBody
    public Result getDataById(Long id){
        TbAddress tbAddress = addressService.getDataById(id);
        Result province = findProvince();
        Result cities = findCities();
        Result areas = findAreas();
        Map<String,Object> map = new HashMap<>();
        map.put("tbAddress",tbAddress);
        map.put("province",province);
        map.put("cities",cities);
        map.put("areas",areas);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(map);
    }

    //修改地址
    @RequestMapping("updateAddress")
    @ResponseBody
    public Result updateAddress(TbAddress tbAddress){
        int i = addressService.updateAddress(tbAddress);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //删除地址
    @RequestMapping("deleteById")
    @ResponseBody
    public Result deleteById(Long id){
        int i = addressService.deleteById(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //查询省
    @RequestMapping("findProvinces")
    @ResponseBody
    public Result findProvince(){
        List<TbProvinces> provincesList = tbProvinceService.findProvince();
        return Result.bulid(ConstantEnum.SUCCESS).setResult(provincesList);
    }


    //查询市
    @RequestMapping("findCities")
    @ResponseBody
    public Result findCities(){
        List<TbCities> citiesList = tbCityService.findCities();
        return Result.bulid(ConstantEnum.SUCCESS).setResult(citiesList);
    }

    //查询区
    @RequestMapping("findAreas")
    @ResponseBody
    public Result findAreas(){
        List<TbAreas> areasList = tbAreasService.findAreas();
        return Result.bulid(ConstantEnum.SUCCESS).setResult(areasList);
    }

    //增加address表
    @RequestMapping("addAddress")
    @ResponseBody
    public Result addAddress(TbAddress tbAddress){
        //获取登录的用户
        TbUser tbUser = (TbUser) SecurityUtils.getSubject().getPrincipal();
        tbAddress.setUserId(tbUser.getUsername());
        tbAddress.setIsDefault("0");
        int i = addressService.addAddress(tbAddress);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //默认地址
    @RequestMapping("changeDefault")
    @ResponseBody
    public Result changeDefault(Long id){
        //获取登录的用户
        TbUser tbUser = (TbUser) SecurityUtils.getSubject().getPrincipal();
        String username = tbUser.getUsername();
        int i = addressService.changeDefault(id,username);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }
}
