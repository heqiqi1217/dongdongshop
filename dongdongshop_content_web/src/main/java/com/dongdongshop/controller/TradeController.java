package com.dongdongshop.controller;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.dongdongshop.config.AlipayConfig;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.po.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("tradeController")
public class TradeController {

    /**
     *跳转到支付页面
     * @param WIDout_trade_no  商户订单号
     * @param WIDtotal_amount  付款金额
     * @param WIDsubject       订单名称
     * @param WIDbody          商品描述
     * @return
     */
    @RequestMapping("toAlipayTradePagePay")
    @ResponseBody
    public String alipayTradePagePay(String WIDout_trade_no, String WIDtotal_amount,
                                     String WIDsubject, String WIDbody) throws AlipayApiException {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        //同步回调路径
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        //异步回调路径
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDout_trade_no +"\","
                + "\"total_amount\":\""+ WIDtotal_amount +"\","
                + "\"subject\":\""+ WIDsubject +"\","
                + "\"body\":\""+ WIDbody +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //请求支付宝,帮我们构建一个form表单,我们拿着这个返回的表单之后直接输出到页面,然后页面就会自动跳转到支付宝的支付页面
        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //输出 jsp中的out.println() == java中的response.getWriter().write() == @ResponseBody
        return result;
    }

    /**
     *交易查询
     * @param WIDTQout_trade_no  商户订单号
     * @param WIDTQtrade_no      支付宝交易号
     * @return
     */
    @RequestMapping("alipayTradeQuery")
    @ResponseBody
    public Result alipayTradeQuery(String WIDTQout_trade_no,String WIDTQtrade_no) throws AlipayApiException {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeQueryRequest alipayRequest = new AlipayTradeQueryRequest();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDTQout_trade_no +"\","+"\"trade_no\":\""+ WIDTQtrade_no +"\"}");

        //请求
        String result = alipayClient.execute(alipayRequest).getBody();

        //统一返回值
        AlipayBaseResult alipayBaseResult = JSONObject.parseObject(result, AlipayBaseResult.class);

        AliPayResultPo response = alipayBaseResult.getAlipay_trade_query_response();
        if(response == null){
            return Result.bulid(ConstantEnum.ERROR).setResult("支付宝查询失败");
        }
        if(response.getCode() != null && response.getCode().intValue() == 10000){
            return Result.bulid(ConstantEnum.SUCCESS).setResult(response);
        }
        return Result.bulid(ConstantEnum.ERROR);
    }

    /**
     *退款
     * @param WIDTRout_trade_no  商户订单号
     * @param WIDTRtrade_no      支付宝交易号
     * @param WIDTRrefund_amount  需要退款的金额
     * @param WIDTRrefund_reason  退款的原因说明
     * @param WIDTRout_request_no 退款请求
     * @return
     */
    @RequestMapping("alipayTradeRefund")
    @ResponseBody
    public Result alipayTradeRefund(String WIDTRout_trade_no,String WIDTRtrade_no,String WIDTRrefund_amount,
                                    String WIDTRrefund_reason,String WIDTRout_request_no){
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeRefundRequest alipayRequest = new AlipayTradeRefundRequest();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDTRout_trade_no +"\","
                + "\"trade_no\":\""+ WIDTRtrade_no +"\","
                + "\"refund_amount\":\""+ WIDTRrefund_amount +"\","
                + "\"refund_reason\":\""+ WIDTRrefund_reason +"\","
                + "\"out_request_no\":\""+ WIDTRout_request_no +"\"}");

        //请求
        String result = null;
        try {
            result = alipayClient.execute(alipayRequest).getBody();
            AlipayTradeRefundBaseResult alipayTradeRefundBaseResult = JSONObject.parseObject(result, AlipayTradeRefundBaseResult.class);
            AlipayTradeRefundResultPo response = alipayTradeRefundBaseResult.getAlipay_trade_refund_response();
            if(response == null){
                return Result.bulid(ConstantEnum.ERROR).setResult("支付宝查询失败");
            }
            if(response != null && response.getCode().intValue() == 10000){
                return Result.bulid(ConstantEnum.SUCCESS).setResult(response);
            }
            return Result.bulid(ConstantEnum.ERROR);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return Result.bulid(ConstantEnum.ERROR);
        }
    }

    /**
     * 退款查询
     * @param WIDRQout_trade_no  商户订单号
     * @param WIDRQtrade_no      支付宝交易号
     * @param WIDRQout_request_no  退款请求号
     * @return
     */
    @RequestMapping("alipayTradeFastpayRefundQuery")
    @ResponseBody
    public Result alipayTradeFastpayRefundQuery(String WIDRQout_trade_no,String WIDRQtrade_no,String WIDRQout_request_no){
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeFastpayRefundQueryRequest alipayRequest = new AlipayTradeFastpayRefundQueryRequest();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDRQout_trade_no +"\","
                +"\"trade_no\":\""+ WIDRQtrade_no +"\","
                +"\"out_request_no\":\""+ WIDRQout_request_no +"\"}");

        //请求
        String result = null;
        try {
            result = alipayClient.execute(alipayRequest).getBody();
           //统一返回值
            AlipayTradeFastpayRefundBaseResult alipayTradeFastpayRefundBaseResult = JSONObject.parseObject(result, AlipayTradeFastpayRefundBaseResult.class);
            AlipayTradeFastpayResultPo response = alipayTradeFastpayRefundBaseResult.getAlipay_trade_fastpay_refund_query_response();
            if(response == null){
                return Result.bulid(ConstantEnum.ERROR).setResult("支付宝查询失败");
            }
            if(response.getCode() != null && response.getCode().intValue() == 10000){
                return Result.bulid(ConstantEnum.SUCCESS).setResult(response);
            }
            return Result.bulid(ConstantEnum.ERROR);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return Result.bulid(ConstantEnum.ERROR);
        }
    }
}
