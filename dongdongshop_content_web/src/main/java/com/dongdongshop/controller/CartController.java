package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.Cart;
import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbOrderItem;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.CartService;
import com.dongdongshop.service.TbOrderItemService;
import com.dongdongshop.service.TbOrderService;
import com.dongdongshop.util.CookieUtils;
import com.dongdongshop.util.IdWorker;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("cartController")
public class CartController {

    @Reference
    private CartService cartService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbOrderService tbOrderService;

    @Reference
    private TbOrderItemService tbOrderItemService;

    //重定向
    @RequestMapping("toMyCart")
    public String toMyCart(){
        return "redirect:/cartController/toCart";
    }

    //跳转页面
    @RequestMapping("toCart")
    public String toCart(){
        return "admin/cart";
    }

    //获取购物车
    @RequestMapping("getCart")
    @ResponseBody
    public List<Cart> getCart(HttpServletRequest request,HttpServletResponse response){
        //获取登录的用户
        TbUser tbUser = (TbUser)SecurityUtils.getSubject().getPrincipal();
        String cookieValue = CookieUtils.getCookieValue(request,"cartList",true);
        if(StringUtils.isBlank(cookieValue)){
            cookieValue = "[]";
        }
        List<Cart> cartListCookie = JSONObject.parseArray(cookieValue, Cart.class);
        if(tbUser == null){  //如果没有登录,就从cookie中获取
            return cartListCookie;
        }else {   //登录了,就从redis中获取
            List<Cart> cartListFromRedis = cartService.getCartListFromRedis(tbUser.getUsername());
            //若cookie中有数据,就合并
            if(cartListCookie.size() > 0){
                //合并购物车
                List<Cart> merageCartList = cartService.merageCartList(cartListCookie, cartListFromRedis);
                //将合并过后的购物车存到redis中
                cartService.addCartListToRedis(merageCartList,tbUser.getUsername());
                //清空cookie中的数据
                CookieUtils.deleteCookie(request,response,"cartList");
                return merageCartList;
            }else {  //若cookie中没有数据,就返回redis中的
                return cartListFromRedis;
            }
        }
    }

    //添加购物车
    @RequestMapping("addCart")
    @ResponseBody
    public List<Cart> addCart(HttpServletRequest request, HttpServletResponse response,Long itemId,Integer num){
        System.out.println("方法执行");
        //先获取购物车
        List<Cart> cartList = getCart(request,response);
        //调用购物车接口,添加购物车数据
        cartList = cartService.addCart(cartList,itemId,num);
        //获取登录的用户
        TbUser tbUser = (TbUser)SecurityUtils.getSubject().getPrincipal();
        if(tbUser == null){   //如果没有登录,就存cookie
            String cartListJson = JSONObject.toJSONString(cartList);
            CookieUtils.setCookie(request,response,"cartList",cartListJson,true);
        }else{   //登录了,就存redis
            cartService.addCartListToRedis(cartList,tbUser.getUsername());
        }
        return cartList;
    }

    //添加购物车,并跳转到购物车页面
    @RequestMapping("addCartPage")
    public String addCartPage(HttpServletRequest request, HttpServletResponse response,Long itemId,Integer num){
        System.out.println("方法执行");
        //先获取购物车
        List<Cart> cartList = getCart(request,response);
        //调用购物车接口,添加购物车数据
        cartList = cartService.addCart(cartList,itemId,num);
        //获取登录的用户
        TbUser tbUser = (TbUser)SecurityUtils.getSubject().getPrincipal();
        if(tbUser == null){   //如果没有登录,就存cookie
            String cartListJson = JSONObject.toJSONString(cartList);
            CookieUtils.setCookie(request,response,"cartList",cartListJson,true);
        }else{   //登录了,就存redis
            cartService.addCartListToRedis(cartList,tbUser.getUsername());
        }
        return "redirect:/cartController/toCart";
    }

    //跳转到结算页面
    @RequestMapping("toGetOrderInfo")
    public String toGetOrderInfo(BigDecimal sum, Model model){
        model.addAttribute("sum",sum);
        return "admin/getOrderInfo";
    }

    //提交订单
    @RequestMapping("submit")
    @ResponseBody
    public Result submit(String contact,String mobile,String address){
        //获取登录的用户
        TbUser tbUser = (TbUser) SecurityUtils.getSubject().getPrincipal();
        //从购物车里拿出来商品
        List<Cart> cartListFromRedis = cartService.getCartListFromRedis(tbUser.getUsername());
        TbOrder tbOrder = new TbOrder();
        TbOrderItem tbOrderItem = new TbOrderItem();
        IdWorker idWorker1 = new IdWorker(0,0);
        Long outTradeNo = idWorker1.nextId(); //生成的商户订单号
        for (int i = 0; i < cartListFromRedis.size(); i++) {
            Cart cart = cartListFromRedis.get(i);
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            IdWorker idWorker = new IdWorker(0,0);
            Long orderId = idWorker.nextId();
            BigDecimal bigDecimal = new BigDecimal(0);
            for (int j = 0; j < orderItemList.size(); j++) {
                Long orderId1 = idWorker.nextId();
                //增加oderItem表
                TbOrderItem tbOrderItem1 = orderItemList.get(j);
                bigDecimal = bigDecimal.add(tbOrderItem1.getTotalFee());
                tbOrderItem.setId(orderId1);
                tbOrderItem.setItemId(tbOrderItem1.getItemId());   //商品id
                tbOrderItem.setGoodsId(tbOrderItem1.getGoodsId()); //spuid
                tbOrderItem.setOrderId(orderId);   //订单id
                tbOrderItem.setTitle(tbOrderItem1.getTitle());  //商品标题
                tbOrderItem.setPrice(tbOrderItem1.getPrice());  //商品单价
                tbOrderItem.setNum(tbOrderItem1.getNum());   //商品购买数量
                tbOrderItem.setTotalFee(tbOrderItem1.getTotalFee()); // 商品总金额
                tbOrderItem.setSellerId(tbOrderItem1.getSellerId()); //商家id
                tbOrderItem.setOutTradeNo(Long.toString(outTradeNo));   //商户订单号
                //增加orderItem表
                cartService.submitOrderItem(tbOrderItem);
            }
            //增加order表
            tbOrder.setOrderId(orderId);   //订单id
            tbOrder.setPayment(bigDecimal);//总价
            tbOrder.setPaymentType("1");  //支付类型
            tbOrder.setStatus("1");    //支付状态
            tbOrder.setCreateTime(new Date());  //订单创建时间
            tbOrder.setUpdateTime(new Date());  //订单更新时间
            tbOrder.setUserId(tbUser.getUsername());  //用户id
            tbOrder.setBuyerNick(tbUser.getUsername()); //买家昵称
            tbOrder.setInvoiceType("3");//发票
            tbOrder.setSourceType("2"); //pc端
            tbOrder.setReceiverAreaName(address);    //收货人地址
            tbOrder.setReceiverMobile(mobile);      //收货人手机号
            tbOrder.setReceiver(contact);  //收货人
            tbOrder.setSellerId(tbOrderItem.getSellerId());    //商家id
            tbOrder.setOutTradeNo(Long.toString(outTradeNo));  //商户订单号
            //增加order表
            cartService.submit(tbOrder);
        }
        redisTemplate.boundHashOps("cartList").delete(tbUser.getUsername());
        return Result.bulid(ConstantEnum.SUCCESS).setResult(Long.toString(outTradeNo));
    }

    //跳转到订单提交成功页面
    @RequestMapping("toPay")
    public String toPay(){
        return "admin/pay";
    }

    //跳转到我的订单页面
    @RequestMapping("myOrder")
    public String myOrder(){
        return "admin/home-index";
    }

    //查询order表
    @RequestMapping("getOrder")
    @ResponseBody
    public Result getOrder(){
        //获取登录的用户
        TbUser tbUser = (TbUser) SecurityUtils.getSubject().getPrincipal();
        //根据登录用户查询order表
        List<TbOrder> tbOrderList = tbOrderService.getOrder(tbUser.getUsername());

        List<Cart> cartList = new ArrayList<>();
        List<TbOrderItem> tbOrderItemList = new ArrayList<>();

        for (int i = 0; i < tbOrderList.size(); i++) {
            Cart cart = new Cart();
            Long orderId = tbOrderList.get(i).getOrderId();
            String sellerId = tbOrderList.get(i).getSellerId();
            Date createTime = tbOrderList.get(i).getCreateTime();
            String outTradeNo = tbOrderList.get(i).getOutTradeNo();
            //根据orderId查询orderitem表
            tbOrderItemList = tbOrderItemService.getOrderItem(orderId);
            cart.setOrderItemList(tbOrderItemList);
            cart.setSellerId(sellerId);
            cart.setCreateTime(createTime);
            cart.setOutTradeNo(outTradeNo);
            cartList.add(cart);
        }
        return Result.bulid(ConstantEnum.SUCCESS).setResult(cartList);
    }
}
