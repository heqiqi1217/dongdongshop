package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.service.ContentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("indexController")
public class IndexController {

    @Reference
    private ContentService contentService;

    //查询广告表
    @RequestMapping("showAll")
    @ResponseBody
    public Result showAll(Long categoryId){
        List<TbContent> list = contentService.showAll(categoryId);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }

    //跳转到我的东东商城页面
    @RequestMapping("toHomeIndex")
    public String toHomeIndex(){
        return "admin/home-index";
    }

    //跳转到待评价页面
    @RequestMapping("toHomeOrderEvaluate")
    public String toHomeOrderEvaluate(){
        return "admin/home-order-evaluate";
    }

    //跳转到待发货页面
    @RequestMapping("toHomeOrderSend")
    public String toHomeOrderSend(){
        return "admin/home-order-send";
    }

    //跳转到地址管理页面
    @RequestMapping("toHomeAddress")
    public String toHomeAddress(){
        return "admin/home-setting-address";
    }

    @RequestMapping("totrade")
    public String totrade(){
        return "trade";
    }
}
