package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.TbUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Controller
@RequestMapping("loginController")
public class LoginController {

    // 发件人的 邮箱 和 密码（替换为自己的邮箱和密码）
    // PS: 某些邮箱服务器为了增加邮箱本身密码的安全性，给 SMTP 客户端设置了独立密码（有的邮箱称为“授权码”）, 对于开启了独立密码的邮箱, 这里的邮箱密码必需使用这个独立密码（授权码）。
    public static String myEmailAccount = "937539220@qq.com";
    public static String myEmailPassword = "chktckwfsugibcif";

    // 发件人邮箱的 SMTP 服务器地址, 必须准确, 不同邮件服务器地址不同, 一般(只是一般, 绝非绝对)格式为: smtp.xxx.com
    // QQ邮箱的 SMTP 服务器地址为: smtp.qq.com
    // 网易163邮箱的 SMTP 服务器地址为: smtp.163.com
    public static String myEmailSMTPHost = "smtp.qq.com";

    // 收件人邮箱（替换为自己知道的有效邮箱）
    public static String receiveMailAccount = "937539220@qq.com";

    @Reference
    private TbUserService tbUserService;

    //转到登录页面
    @RequestMapping("toLogin")
    public String toLogin(){
        return "admin/login";
    }

    //登录
    @RequestMapping("login")
    public String login(String username, String password,Model model) {
        Map<String,Object> map = new HashMap<>();
        //获取subject
        Subject subject = SecurityUtils.getSubject();
        //把pname和pwd封装成token对象
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        //调用subject的login方法
        try{
            subject.login(token);
        }catch (UnknownAccountException e){
            model.addAttribute("message","用户名不存在");
            return "admin/login";
        }catch (IncorrectCredentialsException e){
            model.addAttribute("message","密码不正确");
            return "admin/login";
        }
        return "redirect:/";
    }

    //退出登录
    @RequestMapping("logout")
    public String logout(){
        return "admin/login";
    }

    //跳转到index页面
    @RequestMapping("toIndex")
    public String toIndex(){
        return "index";
    }

    //根据用户名查询tb_user表获取到邮箱
    @RequestMapping("findPassword")
    @ResponseBody
    public Result findPassword(String username){
        TbUser tbUser = tbUserService.findPassword(username);
        System.out.println(tbUser.getEmail());
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbUser);
    }

    //跳转到email页面
    @RequestMapping("toEmail")
    public String toEmail(TbUser tbUser,Model model){
        model.addAttribute("tbUser",tbUser);
        return "admin/email";
    }

    //发送邮件
    @RequestMapping("sendMail")
    public String sendMail(TbUser tbUser) throws Exception {
        // 1. 创建参数配置, 用于连接邮件服务器的参数配置
        Properties props = new Properties();                    // 参数配置
        props.setProperty("mail.transport.protocol", "smtp");   // 使用的协议（JavaMail规范要求）
        props.setProperty("mail.smtp.host", myEmailSMTPHost);   // 发件人的邮箱的 SMTP 服务器地址
        props.setProperty("mail.smtp.auth", "true");            // 需要请求认证

        // 2. 根据配置创建会话对象, 用于和邮件服务器交互
        Session session = Session.getInstance(props);
        session.setDebug(true);                                 // 设置为debug模式, 可以查看详细的发送 log

        //根据username去修改tb_user表中的密码
        String password = tbUserService.updatePassword(tbUser);
        // 3. 创建一封邮件
        MimeMessage message = createMimeMessage(session, myEmailAccount, receiveMailAccount,password);

        // 4. 根据 Session 获取邮件传输对象
        Transport transport = session.getTransport();

        // 5. 使用 邮箱账号 和 密码 连接邮件服务器, 这里认证的邮箱必须与 message 中的发件人邮箱一致, 否则报错
        //    PS: 连接失败的原因通常为以下几点, 仔细检查代码:
        //           (1) 邮箱没有开启 SMTP 服务;
        //           (2) 邮箱密码错误, 例如某些邮箱开启了独立密码;
        //           (3) 邮箱服务器要求必须要使用 SSL 安全连接;
        //           (4) 请求过于频繁或其他原因, 被邮件服务器拒绝服务;
        //           (5) 如果以上几点都确定无误, 到邮件服务器网站查找帮助。
        transport.connect(myEmailAccount, myEmailPassword);

        // 6. 发送邮件, 发到所有的收件地址, message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人, 抄送人, 密送人
        transport.sendMessage(message, message.getAllRecipients());

        // 7. 关闭连接
        transport.close();
        return "admin/login";
    }

    //创建一封邮件
    public static MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail,String password ) throws Exception {
        // 1. 创建一封邮件
        MimeMessage message = new MimeMessage(session);

        // 2. From: 发件人（昵称有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改昵称）
        message.setFrom(new InternetAddress(sendMail, "qq", "UTF-8"));

        // 3. To: 收件人（可以增加多个收件人、抄送、密送）
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "XX用户", "UTF-8"));

        // 4. Subject: 邮件主题（标题有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改标题）
        message.setSubject("哈哈哈", "UTF-8");
        // 5. Content: 邮件正文（可以使用html标签）（内容有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改发送内容）
        message.setContent("修改的密码为:"+password, "text/html;charset=UTF-8");

        // 6. 设置发件时间
        message.setSentDate(new Date());

        // 7. 保存设置
        message.saveChanges();

        // 8. 将该邮件保存到本地
        OutputStream out = new FileOutputStream("E:\\MyEmail.eml");
        message.writeTo(out);
        out.flush();
        out.close();

        return message;
    }
}
