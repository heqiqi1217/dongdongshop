package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.TbUserService;
import com.dongdongshop.util.ShiroUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("registerController")
public class RegisterController {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbUserService tbUserService;

    //转到注册页面
    @RequestMapping("toRegister")
    public String toRegister(){
        return "admin/register";
    }

    //获取验证码,把手机号发送到mq
    @RequestMapping("getMsg")
    @ResponseBody
    public Result getMsg(String phone){
        if(phone == null || "".equals(phone)){
            return Result.bulid(ConstantEnum.ERROR);
        }
        rocketMQTemplate.convertAndSend("phone",phone);//把手机号发送到mq
        return Result.bulid(ConstantEnum.SUCCESS);
    }

    //注册
    @RequestMapping("register")
    @ResponseBody
    public Result register(String username,String password,String pwd,String phone,String msg){
        Map<String,Object> map = new HashMap<>();
        //从redis中取出验证码
        String str = (String)redisTemplate.opsForValue().get(phone);
        if(!Objects.equals(str,msg)){
            map.put("message","验证码输入错误");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }
        //查询出用户表中的所有用户
        List<TbUser> userList = tbUserService.selectAllUser();
        for (int i = 0; i < userList.size(); i++) {
            TbUser tbUser = userList.get(i);
            if(Objects.equals(username,tbUser.getUsername())){
                map.put("message","用户名已存在");
                return Result.bulid(ConstantEnum.ERROR).setResult(map);
            }
        }
        if(StringUtils.isBlank(username)){
            map.put("message","用户名不能为空");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }else if(StringUtils.isBlank(password)){
            map.put("message","密码不能为空");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }else if(StringUtils.isBlank(pwd)){
            map.put("message","确认密码不能为空");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }else if(!Objects.equals(password,pwd)){
            map.put("message","密码和确认密码不一致");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }

        String salt = ShiroUtils.generateSalt(3);
        String md5 = ShiroUtils.encryptPassword("MD5",password, salt, 3);//MD5三次加密之后的密码
        TbUser tbUser = new TbUser();
        tbUser.setUsername(username);
        tbUser.setPassword(md5);
        tbUser.setPhone(phone);
        tbUser.setSalt(salt);
        tbUser.setCreated(new Date());
        tbUser.setUpdated(new Date());

        //注册
        tbUserService.register(tbUser);
        return Result.bulid(ConstantEnum.REGISTER_SUCCESS).setResult(map);
    }
}
