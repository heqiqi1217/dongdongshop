package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.Comment;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.pojo.TbOrderItem;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.CommentService;
import com.dongdongshop.service.TbItemService;
import com.dongdongshop.service.TbOrderItemService;
import com.dongdongshop.service.TbOrderService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/10 11:57
 * @Version
 **/
@Controller
@RequestMapping("commentController")
//跨域的注解
@CrossOrigin
public class CommentController {

    @Reference
    private CommentService commentService;

    @Reference
    private TbItemService tbItemService;

    @Reference
    private TbOrderItemService tbOrderItemService;

    @Reference
    private TbOrderService tbOrderService;

    //跳转到评价页面
    @RequestMapping("toComment")
    public String toComment(Long goodsId,Model model){
        //获取登录的用户
        TbUser tbUser = (TbUser)SecurityUtils.getSubject().getPrincipal();
        model.addAttribute("tbUser",tbUser);
        model.addAttribute("goodsId",goodsId);
        return "admin/comment";
    }

    //增加评论
    @RequestMapping("addComment")
    @ResponseBody
    public Result addComment(Comment comment){
        //根据商品id查询商品标题
        List<TbItem> tbItemList = tbItemService.getTitle(comment.getGoodsId());
        String title = "";
        Long id = 0l;
        Long orderId = 0l;
        for (int i = 0; i < tbItemList.size(); i++) {
            title = tbItemList.get(0).getTitle();
            id = tbItemList.get(0).getId();
        }
        //根据itemId查询tb_order_item表,获取到id
        List<TbOrderItem> tbOrderItemList = tbOrderItemService.selectOrderItemGetId(id);
        for (int j = 0; j < tbOrderItemList.size(); j++) {
            orderId = tbOrderItemList.get(0).getOrderId();
        }
        //根据星级判断评价类型
        if(comment.getStar() > 3 && comment.getStar() <= 5){
            comment.setType("好评");
        }else if(comment.getStar() > 0 && comment.getStar() < 2){
            comment.setType("差评");
        }else if(comment.getStar() == 3){
            comment.setType("中评");
        }
        comment.setDate(new Date());
        comment.setTitle(title);
        //添加评价
        Comment comment1 = commentService.addComment(comment);
        if(comment1 != null){
            //修改订单状态为已评价
            String status = "8";
            tbOrderService.updateStatusByOrderId(orderId,status);
        }
        return Result.bulid(ConstantEnum.SUCCESS).setResult(comment1);
    }

    //查询评价
    @RequestMapping("getComment")
    @ResponseBody
    public Result getComment(Long goodsId, @RequestParam(defaultValue = "3")Integer type){
        List<Comment> list = commentService.getComment(goodsId,type);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }
}
