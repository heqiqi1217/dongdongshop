package com.dongdongshop.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000118621189";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCFIdqHKkivDVONuBUuCpU0gd5E+E6wWQMCPIyeJ/hvGuUSUCvH1yKRlDuDx/vIge5P8KZeZxkooXVQd+P4u2TWX3qB+ORziqbXc51ZEHkgK54ZK0ZmPBn+4kblJrdfS0oiiM79z9mpRDuYadH0en9KcuoWuEstUmCw43G4+M7ehDmCOyNhRU74CRbceJzbAPUzuOqdsjaME7I2BTJUR8f2HklfBJb7jrcYxhZyvyq4Yc1hnNYCtImpwk4JxXc489vevvY99HG3NiOHlNnWHcyQ5KuZLkKbXtjfmT/8mNa/hHVp+mxVkIUUDrYsQNgrJB3tBFE2sFdvsNIThJGKAdaFAgMBAAECggEASXQfPRpQVf0VT4j0bp26KulIeGdECzphVZeQwiYthSeiodMIBM+nLAlMOO5a9gryMLsHH+jkUa2bq5Uqe+7wuPKfBh0L9s/WM+N/OkfDfMN0DFD7G6cndKDmGsKcxAAnXBx9kD5gympyMyqXg7sehjYXAAgYul3MKWykLzlf8hbX/9BR0J3m240esPrqJcy/fckSgYhQBVR6Hmr0SZm20Z8+0acpgDv0m7QlCQ7My29EoNgD3Uf9uKj5UP4rQuH97JwxMG4GejGl9sQ2FRtyS/V9YstLOLDbUKedgnhfWeSphuAnAtCLnCOCpCc/fkp7f7ftAeGBw48lxvIb3EFCdQKBgQDErgyeVO5GpEIQsG6UljxGwkeGgg1Ikkj04CeOGwGbaejgk1C4HW1GK7LkrX1fh63zgIylOQda4BBguYzHoNm1O/lTqoXVIRO5219vPSuivdv6qXVO49TXTGocDDP+0BQVQd5APHcImr2j2W1wEqfuscATL49WkKxlWrAuWN0y3wKBgQCtSTHxISgePkTfqC/fhrJOootzK2tfpvfrgslwDjbp+wsIi8WTf/5rEs4h5WaRRLWw/9Wy8Qb7xBNnBe3g5dZHIMc47RUk9PJf/Aoqrao3FP+Ts6Uru5aUdPCGbRW9314dHSVI6cnB0rv27xtlnveETvzt0lolttUttCLyX0KnGwKBgAmcxAsc9YRIVN5B8TWb3hhLOHmvc3y/gYdOSUCdu0dgNrx7Fq/Y6FVMn3KjtAPuOqcy+iy9C8n6N3Kyvtmm190OsFHRfLb85dzohtKF99L9Wnn2vD/9zJI1Lt5uDkB+5OxhCr6y9dJa1bfjUYDvuHtm/o8xfNzrNzHA6+ln+G+bAoGAP5fUByzm3yp09itv3jU9bJREXvC5R/Irm7d0m3Xr+zRRGULKMFZRlRPiqBfa60bu8tUApNnFeVG82RsY8GcImzPRSPfgwZ5O8KC7TmxLQkmnfZn47LK8ESFVP8/6oaK+lCqlCzwY+dOPfZDEWifbqCegtNp0kNCnLU2lYZNKD68CgYBt7lsdspYwX8gwS6jp2IEDk0bDDVgUfsnP6RDvkL+EUZ+xkV6gfBQM8T/14jnAUdpak6pcXkkvYixslnGzlu4xEJfPLvfWJQHJhkBHLXL7nVBCeUqeTcFOkQ0ldp1CAoxoMpMmzpuL9Sy0GTtS7V00qHEtOdXvyWaSWocrMyxTfw==";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuy8m3sxJC6LA+KkeJka3QXZjm91nGYC2d+tvegfwGNKB7ax6Sxxm+O7I57PZONLy+Tq6N22jg93o4eoK3rmUvoxT5t4K/FLai51PoXBTT9udp/7S8TM+bNIuJUo+SCvIPQhDPtgbKLlJKUaAM8BIfqKHOuisCbsDRbziNw1jPsuCMMNB4rI+95r3k4wgcq8tN+7kzEcowkwlkyvA+YFrSYo2rfkGnLHdErzz3Wn5x0QMg1GYumzssQ6eHbIZSGUhz2ZSDbAoxoroznlfok1F2PiTUAlvNZPKp9RgNOT2VOWcmI4h+fCzml5Qpsb9d5h2whb+bFRxRShC3pllFf6DkwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://4232uk7414.51vip.biz/callBack/notifyUrl";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://4232uk7414.51vip.biz/callBack/returnUrl";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 日志打印路径
    public static String log_path = "E:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
