package com.dongdongshop.data;

import com.dongdongshop.em.ConstantEnum;

public class Result<T> {
    private Integer code;
    private String message;
    private T result;

    public static Result bulid(ConstantEnum e){  //构建result
        Result result = new Result();
        result.setCode(e.getCode());
        result.setMessage(e.getMessage());
        return result;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public Result setResult(T result) {
        this.result = result;
        return this;
    }

    public Result(Integer code, String message, T result) {
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public Result() {
    }
}
