package com.dongdongshop.em;

public enum ConstantEnum {

    SUCCESS(100,"操作成功"),
    REGISTER_SUCCESS(200,"注册成功"),
    UPDATE_SUCCESS(201,"更新成功"),
    ERROR(101,"操作失败"),
    NO_AUTH(102,"未登录");

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ConstantEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    ConstantEnum() {
    }
}
