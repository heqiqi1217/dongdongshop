package com.dongdongshop.service;

import com.dongdongshop.pojo.TbUserScoreAccount;


public interface ScoreAccountService {

    //添加tb_user_score_account表
    void add(TbUserScoreAccount tbUserScoreAccount);

    //根据userId查询积分表,若有该用户,就直接在原来基础上加
    TbUserScoreAccount selectScoreAccountByUserId(String userId);

    //修改tb_user_score_account表
    void update(TbUserScoreAccount tbUserScoreAccount);
}
