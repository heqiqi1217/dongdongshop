package com.dongdongshop.service;

import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbUser;

import java.util.List;

public interface TbOrderService {

    //增加order表
    void submit(TbOrder tbOrder);

    //发送消息给本地事务
    void sendStatus(String out_trade_no, String status, String trade_no);

    //根据登录用户查询order表
    List<TbOrder> getOrder(String userName);

    //修改订单状态为已支付
    void updateStatus(String outTradeNo, String status, String tradeNo);

    //查询order表拿到登录用户
    TbOrder selectUserIdByOutTradeNo(String outTradeNo);

    //修改订单状态为已评价
    void updateStatusByOrderId(Long orderId, String status);
}
