package com.dongdongshop.service;

import com.dongdongshop.pojo.TbCities;
import com.dongdongshop.pojo.TbProvinces;

import java.util.List;

public interface TbProvinceService {

    //查询省
    List<TbProvinces> findProvince();

}
