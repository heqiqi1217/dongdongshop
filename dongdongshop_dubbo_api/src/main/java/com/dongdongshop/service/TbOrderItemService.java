package com.dongdongshop.service;

import com.dongdongshop.pojo.TbOrderItem;

import java.util.List;

public interface TbOrderItemService {

    void submitOrderItem(TbOrderItem tbOrderItem);

    //添加支付宝流水号到orderItem表
    void update(String out_trade_no, String trade_no);

    //根据orderId查询orderitem表
    List<TbOrderItem> getOrderItem(Long orderId);

    //从orderItem表中删除退款的商品
    void delete(String id);

    //根据outTradeNo查询orderItem表,拿到itemId
    TbOrderItem selectOrderItem(String outTradeNo);

    //根据itemId查询tb_order_item表,获取到id
    List<TbOrderItem> selectOrderItemGetId(Long id);
}
