package com.dongdongshop.service;

import com.dongdongshop.pojo.TbUser;

import java.util.Date;
import java.util.List;

public interface TbUserService {
    //登录
    TbUser login(String username);

    //查询出用户表中的所有用户
    List<TbUser> selectAllUser();

    //注冊
    void register(TbUser tbUser);

    //根据用户名查询tb_user表获取到邮箱
    TbUser findPassword(String username);

    //根据username去修改tb_user表中的密码
    String updatePassword(TbUser tbUser);

    List<TbUser> selectUserByDate();

}
