package com.dongdongshop.service;

import com.dongdongshop.pojo.TbSpecificationOption;

import java.util.List;

public interface SpecificationOptionService {

    //回显规格选项名和排序
    List<TbSpecificationOption> selectOptionNameAndOrdersBySpecid(Integer id);
}
