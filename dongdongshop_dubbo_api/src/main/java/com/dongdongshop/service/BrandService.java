package com.dongdongshop.service;

import com.dongdongshop.pojo.TbBrand;

import java.util.List;

public interface BrandService {

    //查询品牌表
    List<TbBrand> brandList();

    //增加品牌表
    int addBrand(TbBrand tbBrand);

    //回显品牌
    TbBrand selectBrandById(Integer id);

    //修改品牌
    int updateBrand(TbBrand tbBrand);

    //批量删除
    int deleteBrandByIds(Integer[] ids);

    TbBrand selectByPrimaryKey(Long brandId);
}
