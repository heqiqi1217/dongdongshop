package com.dongdongshop.service;

import com.dongdongshop.pojo.TbCities;

import java.util.List;

public interface TbCityService {

    //查询市
    List<TbCities> findCities();
}
