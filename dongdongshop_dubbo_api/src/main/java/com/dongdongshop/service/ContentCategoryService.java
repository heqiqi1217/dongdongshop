package com.dongdongshop.service;

import com.dongdongshop.pojo.TbContentCategory;

import java.util.List;

public interface ContentCategoryService {
    //模糊查询广告分类表
    List<TbContentCategory> likeSelect(String name);

    //新建
    int addContentCategory(TbContentCategory tbContentCategory);

    //回显
    TbContentCategory selectById(Long id);

    //修改
    int updateContentCategory(TbContentCategory tbContentCategory);

    //批量删除
    int deletecontentCategoryByIds(Integer[] ids);

}
