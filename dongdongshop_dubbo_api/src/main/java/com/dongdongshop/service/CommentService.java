package com.dongdongshop.service;

import com.dongdongshop.pojo.Comment;

import java.util.List;

public interface CommentService {
    //增加评论
    Comment addComment(Comment comment);

    //查询评价
    List<Comment> getComment(Long goodsId,Integer type);
}
