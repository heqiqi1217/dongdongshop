package com.dongdongshop.service;

import com.dongdongshop.pojo.TbTypeTemplate;

import java.util.List;

public interface TypeTemplateService {

    //查询模板表
    List<TbTypeTemplate> listTypeTemplates(String name);

    //增加模板表
    int addTypeTemplate(TbTypeTemplate tbTypeTemplate);

    //批量删除
    int deleteTypeTemplate(Integer[] ids);

    //回显
    TbTypeTemplate selectTypeTemplateById(Integer id);

    //修改
    int updateTypeTemplate(TbTypeTemplate tbTypeTemplate);
}
