package com.dongdongshop.service;

import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.pojo.TbTypeTemplate;

import java.util.List;

public interface TbSellerService {
    //先查询出商家表中的所有商家
    List<TbSeller> selectAllTbSeller();

    //注册新商家
    void register(TbSeller tbSeller);

    //登录
    TbSeller login(String username);

    //商家管理(加模糊查询)
    List<TbSeller> listSeller(String name,String nickName,String status);

    //详情
    TbSeller listSellerBySellerId(String sellerId);

    //审核通过
    int approvedBySellerId(String sellerId);

    //审核未通过
    int notApprovedBySellerId(String sellerId);

    //关闭商家
    int closeBySellerId(String sellerId);

    //根据模板表的模板id查询规格表
    TbTypeTemplate selectTypeTemplate(Integer id);

    //根据规格表的id查询规格选项表
    List<TbSpecificationOption> selectSpecificationOption(Long id);
}
