package com.dongdongshop.service;

import com.dongdongshop.pojo.Cart;
import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbOrderItem;

import java.util.List;

public interface CartService {

    //调用购物车接口,添加购物车数据
    List<Cart> addCart(List<Cart> cartList, Long itemId, Integer num);

    //将购物车数据加入到redis中
    void addCartListToRedis(List<Cart> cartList,String username);

    //从redis中获取购物车数据
    List<Cart> getCartListFromRedis(String username);

    //合并购物车(合并cookie和redis中的购物车)
    List<Cart> merageCartList(List<Cart> cookieList,List<Cart> redisList);

    //增加order表
    void submit(TbOrder tbOrder);

    //增加orderItem表
    void submitOrderItem(TbOrderItem tbOrderItem);
}
