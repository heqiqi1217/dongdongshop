package com.dongdongshop.service;

import com.dongdongshop.pojo.TbAreas;

import java.util.List;

public interface TbAreasService {

    //查询区
    List<TbAreas> findAreas();
}
