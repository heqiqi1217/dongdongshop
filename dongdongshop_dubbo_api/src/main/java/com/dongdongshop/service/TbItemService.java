package com.dongdongshop.service;

import com.dongdongshop.pojo.TbItem;

import java.util.List;

public interface TbItemService {
    //根据goodsId查询tbItem表
    List<TbItem> selectItemListByGoodsId(Long goodsId);

    //根据itemId(主键id)查询item表
    TbItem selectItemById(Long itemId);

    //再根据itemId修改item表中的num的数量
    void updateNum(Long itemId);

    //搜索
    List<TbItem> getAll(String name, Integer startIndex,Integer pageSize);

    //根据商品id查询商品标题
    List<TbItem> getTitle(Long goodsId);
}
