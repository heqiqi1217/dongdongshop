package com.dongdongshop.service;

import com.dongdongshop.pojo.Page;
import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbGoodsDesc;
import com.dongdongshop.pojo.TbItem;

import java.math.BigDecimal;
import java.util.List;

public interface GoodService {

    //保存
    int save(TbGoods tbGoods, TbGoodsDesc tbGoodsDesc,String listJson,String name,String title,String listSpecJson);

    //展示商品(加模糊查询)
    Page<List<TbGoods>> listGoods(String auditStatus,String goodsName, Integer pageNum, Integer pageSize);

    //展示详情
    TbGoodsDesc listGoodsDesc(Integer id);

    //批量删除
    int deleteGoodsByIds(Long[] ids);

    //批量修改审核状态
    int updateStatus(Long[] ids, Integer auditStatus);

    //查询商品表goods
    List<TbGoods> selectGoods();

    //根据goodsId查询goods表
    TbGoods selectGoodsByGoodsId(Long goodsId);
}
