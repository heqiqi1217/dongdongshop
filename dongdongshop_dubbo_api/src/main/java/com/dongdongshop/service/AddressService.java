package com.dongdongshop.service;

import com.dongdongshop.pojo.TbAddress;

import java.util.List;

public interface AddressService {

    //增加地址表
    int addAddress(TbAddress tbAddress);

    //展示地址
    List<TbAddress> getData(String userName);

    //删除地址
    int deleteById(Long id);

    //回显地址
    TbAddress getDataById(Long id);

    //修改地址
    int updateAddress(TbAddress tbAddress);

    //默认地址
    int changeDefault(Long id,String username);
}
