package com.dongdongshop.service;

import com.dongdongshop.pojo.TbItemCat;

import java.util.List;

public interface TbItemCatService {

    //展示分类表
    List<TbItemCat> listTbItemCat(Long id);

    //拼接导航栏
    List<TbItemCat> getNavigation(Integer ids);

    //批量删除
    int deleteTbItemCat(Integer[] ids);

    //增加
    int addTbItemCat(TbItemCat tbItemCat);

    //回显
    TbItemCat selectTbItemCatById(Integer id);

    //修改
    int updateTbItemCat(TbItemCat tbItemCat);

    //查询tbItemCat表(三级联动用)
    List<TbItemCat> selectItemCat();

    //根据模板表的主键id查询出对应的模板id
    TbItemCat getTypeId(Integer id);

    TbItemCat selectByPrimaryKey(Long category3Id);
}
