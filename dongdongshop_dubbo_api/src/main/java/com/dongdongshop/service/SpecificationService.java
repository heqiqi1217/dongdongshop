package com.dongdongshop.service;

import com.dongdongshop.pojo.TbSpecification;

import java.util.List;

public interface SpecificationService {

    //查询规格表
    List<TbSpecification> listSpecification(String spceName);

    //增加规格表和规格选项表
    int addSpecification(TbSpecification tbSpecification,String optionListJson);

    //回显规格表和规格选项表
    TbSpecification selectSpecificationById(Integer id);

    //修改规格表和规格选项表
    int updateSpecificationById(TbSpecification tbSpecification, String updateOptionListJson);

    //批量删除规格表和规格选项表
    int deleteSpecificationByIds(Integer[] ids);
}
