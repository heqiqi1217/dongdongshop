package com.dongdongshop.service;

import com.dongdongshop.pojo.TbGoodsDesc;

import java.util.List;

public interface GoodDescService {

    TbGoodsDesc selectGoodsListByGoodsId(Long goodsId);
}
