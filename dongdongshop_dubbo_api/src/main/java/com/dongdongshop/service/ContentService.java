package com.dongdongshop.service;

import com.dongdongshop.pojo.TbContent;

import java.util.List;

public interface ContentService {

    //展示广告表
    List<TbContent> selectContent();

    //保存
    int addContent(TbContent tbContent);

    //批量删除
    int deleteContentByIds(Integer[] ids);

    //回显广告
    TbContent selectContentByid(Integer id);

    //修改
    int updateContentByid(TbContent tbContent);

    //查询广告表
    List<TbContent> showAll(Long categoryId);
}
