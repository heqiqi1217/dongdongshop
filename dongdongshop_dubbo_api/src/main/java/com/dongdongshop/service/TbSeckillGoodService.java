package com.dongdongshop.service;

import com.dongdongshop.pojo.TbSeckillGoods;

import java.util.List;

public interface TbSeckillGoodService {
    //查询tb_seckill_goods表
    List<TbSeckillGoods> findGoods();

    //跳转到seckill-item页面,抢购页面
    TbSeckillGoods findGoodsById(Long id);

    //创建订单
    void createOrder(Long goodsId, String username) throws Exception;

    //查询订单号
    Long getOrderId(String username);

    //查询所有秒杀商品
    List<TbSeckillGoods> selectAllGoods();

    //往redis里增加
    void addGoodsToRedis(long parseLong);

    //删除redis中的数据
    void deleteRedisGoods(long parseLong);
}
