package com.dongdongshop.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Cart implements Serializable {

    private String sellerId;  //商家id
    private List<TbOrderItem> orderItemList;  //购物车明细列表

    private Date createTime; //订单创建时间

    private String outTradeNo; //订单编号

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public List<TbOrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<TbOrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public Cart() {
    }

    public Cart(String sellerId, List<TbOrderItem> orderItemList) {
        this.sellerId = sellerId;
        this.orderItemList = orderItemList;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }
}
