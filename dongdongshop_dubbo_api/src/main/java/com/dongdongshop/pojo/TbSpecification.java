package com.dongdongshop.pojo;

import java.io.Serializable;

public class TbSpecification implements Serializable {
    private Long id;

    private String specName;

    //业务字段
    private String text;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName == null ? null : specName.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}