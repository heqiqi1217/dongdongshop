package com.dongdongshop.pojo;

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable {
    private Integer pageNum;//当前页数
    private Integer pageSize;//每页条数
    private Long total;//总条数
    private Integer pages;//总页数
    private List<T> rows; //数据

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public Page(Integer pageNum, Integer pageSize, Long total, Integer pages, List<T> rows) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = total;
        this.pages = pages;
        this.rows = rows;
    }

    public Page() {
    }
}
