package com.dongdongshop.pojo;

import java.io.Serializable;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/8 17:33
 * @Version
 **/
public class WeatherVO implements Serializable {

    private DataVO data;

    private String status;

    private String desc;

    public DataVO getData() {
        return data;
    }

    public void setData(DataVO data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public WeatherVO(DataVO data, String status, String desc) {
        this.data = data;
        this.status = status;
        this.desc = desc;
    }

    public WeatherVO() {
    }

    @Override
    public String toString() {
        return "WeatherVO{" +
                "data='" + data + '\'' +
                ", status='" + status + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
