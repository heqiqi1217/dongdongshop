package com.dongdongshop.pojo;

import java.io.Serializable;
import java.util.Date;

public class TbUserScoreAccount implements Serializable {
    private Long id;

    private String userId;

    private Long totalScore;

    private Long expendScore;

    private Date createTime;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Long getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Long totalScore) {
        this.totalScore = totalScore;
    }

    public Long getExpendScore() {
        return expendScore;
    }

    public void setExpendScore(Long expendScore) {
        this.expendScore = expendScore;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}