package com.dongdongshop.pojo;

import java.io.Serializable;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/8 17:40
 * @Version
 **/
public class DataVO implements Serializable {

    private String yesterday;

    private String city;

    private String forecast;

    private String ganmao;

    private String wendu;

    public String getYesterday() {
        return yesterday;
    }

    public void setYesterday(String yesterday) {
        this.yesterday = yesterday;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getForecast() {
        return forecast;
    }

    public void setForecast(String forecast) {
        this.forecast = forecast;
    }

    public String getGanmao() {
        return ganmao;
    }

    public void setGanmao(String ganmao) {
        this.ganmao = ganmao;
    }

    public String getWendu() {
        return wendu;
    }

    public void setWendu(String wendu) {
        this.wendu = wendu;
    }

    public DataVO(String yesterday, String city, String forecast, String ganmao, String wendu) {
        this.yesterday = yesterday;
        this.city = city;
        this.forecast = forecast;
        this.ganmao = ganmao;
        this.wendu = wendu;
    }

    public DataVO() {
    }

    @Override
    public String toString() {
        return "DataVO{" +
                "yesterday='" + yesterday + '\'' +
                ", city='" + city + '\'' +
                ", forecast='" + forecast + '\'' +
                ", ganmao='" + ganmao + '\'' +
                ", wendu='" + wendu + '\'' +
                '}';
    }
}
