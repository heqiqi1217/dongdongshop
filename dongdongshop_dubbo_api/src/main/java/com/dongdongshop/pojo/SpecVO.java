package com.dongdongshop.pojo;

import java.io.Serializable;
import java.util.List;

public class SpecVO implements Serializable {

    private TbSpecification tbSpecification;
    private List<TbSpecificationOption> list;

    public TbSpecification getTbSpecification() {
        return tbSpecification;
    }

    public void setTbSpecification(TbSpecification tbSpecification) {
        this.tbSpecification = tbSpecification;
    }

    public List<TbSpecificationOption> getList() {
        return list;
    }

    public void setList(List<TbSpecificationOption> list) {
        this.list = list;
    }

    public SpecVO(TbSpecification tbSpecification, List<TbSpecificationOption> list) {
        this.tbSpecification = tbSpecification;
        this.list = list;
    }

    public SpecVO() {
    }
}
