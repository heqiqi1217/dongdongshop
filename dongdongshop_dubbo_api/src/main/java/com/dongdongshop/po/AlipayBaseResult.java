package com.dongdongshop.po;

import java.io.Serializable;

/**
 *交易查询的类
 */
public class AlipayBaseResult implements Serializable {

    private AliPayResultPo alipay_trade_query_response;

    private String sign;

    public AliPayResultPo getAlipay_trade_query_response() {
        return alipay_trade_query_response;
    }

    public void setAlipay_trade_query_response(AliPayResultPo alipay_trade_query_response) {
        this.alipay_trade_query_response = alipay_trade_query_response;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
