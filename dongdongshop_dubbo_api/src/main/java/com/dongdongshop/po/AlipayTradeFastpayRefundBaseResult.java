package com.dongdongshop.po;

import java.io.Serializable;

/**
 * 退款查询的类
 */
public class AlipayTradeFastpayRefundBaseResult implements Serializable {

    private AlipayTradeFastpayResultPo alipay_trade_fastpay_refund_query_response;

    private String sign;

    public AlipayTradeFastpayResultPo getAlipay_trade_fastpay_refund_query_response() {
        return alipay_trade_fastpay_refund_query_response;
    }

    public void setAlipay_trade_fastpay_refund_query_response(AlipayTradeFastpayResultPo alipay_trade_fastpay_refund_query_response) {
        this.alipay_trade_fastpay_refund_query_response = alipay_trade_fastpay_refund_query_response;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
