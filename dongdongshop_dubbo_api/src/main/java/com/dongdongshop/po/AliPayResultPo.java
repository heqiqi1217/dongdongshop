package com.dongdongshop.po;

import java.io.Serializable;

/** 交易查询的po类
 * 调用支付宝接收统一返回值用的
 * pojo:实体类的统称
 * entity:表示与数据库打交道的实体类
 * po:与三方项目交互定义的实体
 * vo:前后端数据交互定义的尸体
 */
public class AliPayResultPo implements Serializable {

    private Integer code;

    private String msg;

    private String buyer_logon_id;

    private String buyer_pay_amount;

    private String buyer_user_id;

    private String buyer_user_name;

    private String buyer_user_type;

    private String invoice_amount;

    private String out_trade_no;

    private String point_amount;

    private String receipt_amount;

    private String send_pay_date;

    private String total_amount;

    private String trade_no;

    private String trade_status;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getBuyer_logon_id() {
        return buyer_logon_id;
    }

    public void setBuyer_logon_id(String buyer_logon_id) {
        this.buyer_logon_id = buyer_logon_id;
    }

    public String getBuyer_pay_amount() {
        return buyer_pay_amount;
    }

    public void setBuyer_pay_amount(String buyer_pay_amount) {
        this.buyer_pay_amount = buyer_pay_amount;
    }

    public String getBuyer_user_id() {
        return buyer_user_id;
    }

    public void setBuyer_user_id(String buyer_user_id) {
        this.buyer_user_id = buyer_user_id;
    }

    public String getBuyer_user_name() {
        return buyer_user_name;
    }

    public void setBuyer_user_name(String buyer_user_name) {
        this.buyer_user_name = buyer_user_name;
    }

    public String getBuyer_user_type() {
        return buyer_user_type;
    }

    public void setBuyer_user_type(String buyer_user_type) {
        this.buyer_user_type = buyer_user_type;
    }

    public String getInvoice_amount() {
        return invoice_amount;
    }

    public void setInvoice_amount(String invoice_amount) {
        this.invoice_amount = invoice_amount;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getPoint_amount() {
        return point_amount;
    }

    public void setPoint_amount(String point_amount) {
        this.point_amount = point_amount;
    }

    public String getReceipt_amount() {
        return receipt_amount;
    }

    public void setReceipt_amount(String receipt_amount) {
        this.receipt_amount = receipt_amount;
    }

    public String getSend_pay_date() {
        return send_pay_date;
    }

    public void setSend_pay_date(String send_pay_date) {
        this.send_pay_date = send_pay_date;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public String getTrade_status() {
        return trade_status;
    }

    public void setTrade_status(String trade_status) {
        this.trade_status = trade_status;
    }
}
