package com.dongdongshop.po;

import java.io.Serializable;

/**
 * 退款的类
 */
public class AlipayTradeRefundBaseResult implements Serializable {

    private AlipayTradeRefundResultPo alipay_trade_refund_response;

    private String sign;

    public AlipayTradeRefundResultPo getAlipay_trade_refund_response() {
        return alipay_trade_refund_response;
    }

    public void setAlipay_trade_refund_response(AlipayTradeRefundResultPo alipay_trade_refund_response) {
        this.alipay_trade_refund_response = alipay_trade_refund_response;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
