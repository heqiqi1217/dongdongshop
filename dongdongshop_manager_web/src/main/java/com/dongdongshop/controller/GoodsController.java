package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.Page;
import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbGoodsDesc;
import com.dongdongshop.service.GoodService;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("goodsController")
public class GoodsController {

    @Reference
    private GoodService goodService;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    //转到商品审核页面
    @RequestMapping("toGoods")
    public String toGoods(){
        return "admin/goods";
    }

    //展示商品(模糊查询加分页)
    @RequestMapping("listGoods")
    @ResponseBody
    public Result listGoods(String auditStatus,String goodsName,@RequestParam(defaultValue = "1") Integer pageNum,@RequestParam(defaultValue = "10") Integer pageSize){
        Page<List<TbGoods>> goodsList = goodService.listGoods(auditStatus,goodsName,pageNum,pageSize);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(goodsList);
    }

    //展示详情
    @RequestMapping("listGoodsDesc")
    @ResponseBody
    public Result listGoodsDesc(Integer id){
        TbGoodsDesc goodsDesc = goodService.listGoodsDesc(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(goodsDesc);
    }

    //批量修改审核状态
    @RequestMapping("updateStatus")
    @ResponseBody
    public Result updateStatus(Long[] ids,Integer auditStatus){
        int i = goodService.updateStatus(ids,auditStatus);
        for (int j = 0; j < ids.length; j++) {
            rocketMQTemplate.convertAndSend("id",ids[j]);  //生产者发送消息
        }
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //批量删除
    @RequestMapping("deleteGoodsByIds")
    @ResponseBody
    public Result deleteGoodsByIds(Long[] ids){
        int i = goodService.deleteGoodsByIds(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //测试mq
    /*@RequestMapping("sendMsg")
    @ResponseBody
    public String sendMsg(){
        rocketMQTemplate.convertAndSend("ids","hellow");
        return "ok";
    }*/
}
