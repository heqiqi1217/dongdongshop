package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.pojo.TbContentCategory;
import com.dongdongshop.service.ContentCategoryService;
import com.dongdongshop.service.ContentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Controller
@RequestMapping("contentController")
public class ContentController {

    @Reference
    private ContentCategoryService contentCategoryService;

    @Reference
    private ContentService contentService;

    //转到广告管理页面
    @RequestMapping("toContent")
    public String toContent(Model model){
        List<TbContentCategory> contentCategoryList = contentCategoryService.likeSelect(null);
        model.addAttribute("contentCategoryList",contentCategoryList);
        return "admin/content";
    }

    //展示广告表
    @RequestMapping("selectContent")
    @ResponseBody
    public Result selectContent(){
        List<TbContent> contentList = contentService.selectContent();
        return Result.bulid(ConstantEnum.SUCCESS).setResult(contentList);
    }

    //文件上传
    @RequestMapping ("uploadFile")
    @ResponseBody
    public Map<String,String> uploadFile(@RequestParam("myFile")MultipartFile myFile){

        Map<String,String> maps = new HashMap();
        // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
        String http = "https://";
        String bucketName = "dongdongshop-hqq";
        String endpoint = "oss-cn-beijing.aliyuncs.com";//端点
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = "LTAI5tKqSkBxTE5BbYcfHkMg";
        String accessKeySecret = "SDozOgebvZBF5sKF1yupJhNi39Rdzs";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(http+endpoint, accessKeyId, accessKeySecret);

        // 填写字符串。
        //String content = "Hello OSS";
        String fileName= null;
        fileName= "content/" + UUID.randomUUID()+".jpg";
        // 创建PutObjectRequest对象。
        // 依次填写Bucket名称（例如examplebucket）和Object完整路径（例如exampledir/exampleobject.txt）。Object完整路径中不能包含Bucket名称。new ByteArrayInputStream(content.getBytes())
        try {
            InputStream stream = myFile.getInputStream();
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName,stream);
            ossClient.putObject(putObjectRequest);
        }catch (IOException e){
            System.out.println("上传失败");
            e.printStackTrace();
        }

        // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
        // ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
        // metadata.setObjectAcl(CannedAccessControlList.Private);
        // putObjectRequest.setMetadata(metadata);

        // 上传字符串。

        // 关闭OSSClient。
        ossClient.shutdown();

        maps.put("imgUrl",http+bucketName+"."+endpoint+"/"+fileName);
        return maps;
    }


    //保存
    @RequestMapping("addContent")
    @ResponseBody
    public Result addContent(TbContent tbContent){
        int i = contentService.addContent(tbContent);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //批量删除
    @RequestMapping("deleteContentByIds/{ids}")
    @ResponseBody
    public Result deleteContentByIds(@PathVariable("ids")Integer[] ids){
        int i = contentService.deleteContentByIds(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //回显广告
    @RequestMapping("selectContentByid")
    @ResponseBody
    public Result selectContentByid(Integer id){
        TbContent tbContent = contentService.selectContentByid(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbContent);
    }

    //修改广告
    @RequestMapping("updateContentByid")
    @ResponseBody
    public Result updateContentByid(TbContent tbContent){
        int i = contentService.updateContentByid(tbContent);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }


}
