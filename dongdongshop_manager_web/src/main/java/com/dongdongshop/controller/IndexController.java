package com.dongdongshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    //转到index页面
    @RequestMapping(path={"index","/"})
    public String index(){
        return "admin/index";
    }
}
