package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.service.TbSellerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("sellerController")
public class SellerController {

    @Reference
    private TbSellerService tbSellerService;

    //转到商家管理页面
    @RequestMapping("toSeller")
    public String toSeller1(){
        //return "admin/seller_1";
        return "admin/seller";
    }

    //商家管理(加模糊查询)
    @RequestMapping("listSeller")
    @ResponseBody
    public Result listSeller(String name,String nickName,String status){
        List<TbSeller> list = tbSellerService.listSeller(name,nickName,status);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }

    //详情
    @RequestMapping("listSellerBySellerId")
    @ResponseBody
    public Result listSellerBySellerId(String sellerId){
        TbSeller tbSeller = tbSellerService.listSellerBySellerId(sellerId);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbSeller);
    }

    //审核通过
    @RequestMapping("approvedBySellerId")
    @ResponseBody
    public Result approvedBySellerId(String sellerId){
        int i = tbSellerService.approvedBySellerId(sellerId);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //审核未通过
    @RequestMapping("notApprovedBySellerId")
    @ResponseBody
    public Result notApprovedBySellerId(String sellerId){
        int i = tbSellerService.notApprovedBySellerId(sellerId);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //关闭商家
    @RequestMapping("closeBySellerId")
    @ResponseBody
    public Result closeBySellerId(String sellerId){
        int i = tbSellerService.closeBySellerId(sellerId);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }
}
