package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbItemCat;
import com.dongdongshop.pojo.TbTypeTemplate;
import com.dongdongshop.service.TbItemCatService;
import com.dongdongshop.service.TypeTemplateService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("tbItemCat")
public class TbItemCatController {

    @Reference
    private TbItemCatService tbItemCatService;

    @Reference
    private TypeTemplateService typeTemplateService;

    //转到展示分类管理页面
    @RequestMapping("list")
    public String list(Model model){
        List<TbTypeTemplate> tbTypeTemplates = typeTemplateService.listTypeTemplates("");
        model.addAttribute("tbTypeTemplates",tbTypeTemplates);
        return "admin/item_cat";
    }

    //展示分类表
    @RequestMapping("listTbItemCat")
    @ResponseBody
    public List<TbItemCat> listTbItemCat(@RequestParam(defaultValue = "0")Long id){
        List<TbItemCat> list = tbItemCatService.listTbItemCat(id);
        return list;
    }

    //拼接导航栏
    @RequestMapping("getNavigation")
    @ResponseBody
    public Result getNavigation(Integer ids){
        List<TbItemCat> list = tbItemCatService.getNavigation(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }

    //增加分类表
    @RequestMapping("addTbItemCat")
    @ResponseBody
    public Result addTbItemCat(TbItemCat tbItemCat){
        int i = tbItemCatService.addTbItemCat(tbItemCat);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //回显分类表
    @RequestMapping("selectTbItemCatById")
    @ResponseBody
    public Result selectTbItemCatById(Integer id){
        TbItemCat tbItemCat = tbItemCatService.selectTbItemCatById(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbItemCat);
    }

    //修改分类表
    @RequestMapping("updateTbItemCat")
    @ResponseBody
    public Result updateTbItemCat(TbItemCat tbItemCat){
        int i = tbItemCatService.updateTbItemCat(tbItemCat);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //批量删除
    @RequestMapping("deleteTbItemCat")
    @ResponseBody
    public Result deleteTbItemCat(Integer[] ids){
        int i = tbItemCatService.deleteTbItemCat(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }
}
