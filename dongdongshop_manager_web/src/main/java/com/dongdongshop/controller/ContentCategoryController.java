package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbContentCategory;
import com.dongdongshop.service.ContentCategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("contentCategoryController")
public class ContentCategoryController {

    @Reference
    private ContentCategoryService contentCategoryService;

    //转到content_category页面
    @RequestMapping("toContentCategory")
    public String toContentCategory(){
        return "admin/content_category";
    }

    //模糊查询广告分类表
    @RequestMapping("likeSelect")
    @ResponseBody
    public Result likeSelect(String name){
        List<TbContentCategory> list = contentCategoryService.likeSelect(name);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }

    //新建
    @RequestMapping("addContentCategory")
    @ResponseBody
    public Result addContentCategory(TbContentCategory tbContentCategory){
        int i = contentCategoryService.addContentCategory(tbContentCategory);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //回显
        @RequestMapping("selectById")
    @ResponseBody
    public Result selectById(Long id){
        TbContentCategory tbContentCategory = contentCategoryService.selectById(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbContentCategory);
    }

    //修改
    @RequestMapping("updateContentCategory")
    @ResponseBody
    public Result updateContentCategory(TbContentCategory tbContentCategory){
        int i = contentCategoryService.updateContentCategory(tbContentCategory);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //批量删除
    @RequestMapping("deletecontentCategoryByIds/{ids}")
    @ResponseBody
    public Result deletecontentCategoryByIds(@PathVariable("ids")Integer[] ids){
        int i = contentCategoryService.deletecontentCategoryByIds(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

}
