package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbBrand;
import com.dongdongshop.service.BrandService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("brand")
public class BrandController {

    @Reference
    private BrandService brandService;

    //转到展示品牌表
    @RequestMapping("list")
    public String list(){
        return "admin/brand";
    }

    //查询品牌表
    @RequestMapping("listBrands")
    @ResponseBody
    public Result listBrands(){
        List<TbBrand> brandList = brandService.brandList();
        return Result.bulid(ConstantEnum.SUCCESS).setResult(brandList);
    }

    //增加品牌表
    @RequestMapping("addBrand")
    @ResponseBody
    public Result addBrand(TbBrand tbBrand){
        int i = brandService.addBrand(tbBrand);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //回显品牌
    @RequestMapping("selectBrandById")
    @ResponseBody
    public Result selectBrandById(Integer id){
        TbBrand tbBrand = brandService.selectBrandById(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbBrand);
    }

    //修改品牌
    @RequestMapping("updateBrand")
    @ResponseBody
    public Result updateBrand(TbBrand tbBrand){
        int i = brandService.updateBrand(tbBrand);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }


    //批量删除
    @RequestMapping("deleteBrandByIds/{ids}")
    @ResponseBody
    public Result deleteBrandByIds(@PathVariable("ids") Integer[] ids){
        int i = brandService.deleteBrandByIds(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }
}

