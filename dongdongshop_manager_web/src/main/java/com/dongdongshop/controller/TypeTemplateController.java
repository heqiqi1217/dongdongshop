package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbTypeTemplate;
import com.dongdongshop.service.TypeTemplateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("typeTemplate")
public class TypeTemplateController {

    @Reference
    private TypeTemplateService typeTemplateService;

    //转到展示模板表
    @RequestMapping("list")
    public String list(){
        return "admin/type_template";
    }

    //查询模板表
    @RequestMapping("listTypeTemplates")
    @ResponseBody
    public Result listTypeTemplates(String name){
        List<TbTypeTemplate> list = typeTemplateService.listTypeTemplates(name);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }

    //增加模板表
    @RequestMapping("addTypeTemplate")
    @ResponseBody
    public Result addTypeTemplate(String name,String brandsArrJson,String specsArrJson,String customAttributeItemsListJson){
        TbTypeTemplate tbTypeTemplate = new TbTypeTemplate();
        tbTypeTemplate.setName(name);
        tbTypeTemplate.setBrandIds(brandsArrJson);
        tbTypeTemplate.setSpecIds(specsArrJson);
        tbTypeTemplate.setCustomAttributeItems(customAttributeItemsListJson);
        int i = typeTemplateService.addTypeTemplate(tbTypeTemplate);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //批量删除
    @RequestMapping("deleteTypeTemplate/{ids}")
    @ResponseBody
    public Result deleteTypeTemplate(@PathVariable("ids")Integer[] ids){
        int i = typeTemplateService.deleteTypeTemplate(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //回显
    @RequestMapping("selectTypeTemplateById")
    @ResponseBody
    public Result selectTypeTemplateById(Integer id){
        TbTypeTemplate tbTypeTemplate = typeTemplateService.selectTypeTemplateById(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbTypeTemplate);
    }

    //修改
    @RequestMapping("updateTypeTemplate")
    @ResponseBody
    public Result updateTypeTemplate(Integer id,String name,String updateBrandsArrJson,String updateSpecsArrJson,String customAttributeItemsListJson){
        TbTypeTemplate tbTypeTemplate = new TbTypeTemplate();
        tbTypeTemplate.setId(id.longValue());
        tbTypeTemplate.setName(name);
        tbTypeTemplate.setBrandIds(updateBrandsArrJson);
        tbTypeTemplate.setSpecIds(updateSpecsArrJson);
        tbTypeTemplate.setCustomAttributeItems(customAttributeItemsListJson);
        int i = typeTemplateService.updateTypeTemplate(tbTypeTemplate);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }
}
