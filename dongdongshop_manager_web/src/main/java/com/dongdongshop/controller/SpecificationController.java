package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.SpecVO;
import com.dongdongshop.pojo.TbSpecification;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.service.SpecificationOptionService;
import com.dongdongshop.service.SpecificationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("specification")
public class SpecificationController {

    @Reference
    private SpecificationService specificationService;

    @Reference
    private SpecificationOptionService specificationOptionService;

    //转到展示规格表
    @RequestMapping("list")
    public String list(){
        return "admin/specification";
    }

    //查询规格表(加模糊查询)
    @RequestMapping("listSpecification")
    @ResponseBody
    public Result listSpecification(String spceName){
        List<TbSpecification> specificationList = specificationService.listSpecification(spceName);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(specificationList);
    }

    //增加规格表和规格选项表
    @RequestMapping("addSpecification")
    @ResponseBody
    public Result addSpecification(TbSpecification tbSpecification,String optionListJson){
        int i = specificationService.addSpecification(tbSpecification,optionListJson);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //回显规格表和规格选项表
    @RequestMapping("selectSpecificationById")
    @ResponseBody
    public Result selectSpecificationById(Integer id){
        //回显规格名称
        TbSpecification tbSpecification = specificationService.selectSpecificationById(id);

        //回显规格选项名和排序
        List<TbSpecificationOption> list = specificationOptionService.selectOptionNameAndOrdersBySpecid(id);

        //把查询的结果放到VO类中
        SpecVO specVO = new SpecVO(tbSpecification,list);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(specVO);
    }

    //修改规格表和规格选项表
    @RequestMapping("updateSpecificationById")
    @ResponseBody
    public Result updateSpecificationById(TbSpecification tbSpecification,String updateOptionListJson){
        int i = specificationService.updateSpecificationById(tbSpecification,updateOptionListJson);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //批量删除规格表和规格选项表
    @RequestMapping("deleteSpecificationByIds/{ids}")
    @ResponseBody
    public Result deleteSpecificationByIds(@PathVariable("ids")Integer[] ids){
        int i = specificationService.deleteSpecificationByIds(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }
}
