package com.dongdongshop.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.service.TbItemService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ItemServiceImpl implements TbItemService {

    @Autowired
    private RestHighLevelClient client;

    @Override
    public List<TbItem> selectItemListByGoodsId(Long goodsId) {
        return null;
    }

    @Override
    public TbItem selectItemById(Long itemId) {
        return null;
    }

    @Override
    public void updateNum(Long itemId) {

    }

    //搜索
    @Override
    public List<TbItem> getAll(String name, Integer startIndex,Integer pageSize) {
        List<TbItem> tbItemList = new ArrayList<>();
        //1.构建SearchRequest请求对象,指定索引库
        SearchRequest searchRequest = new SearchRequest("dongdongshop");
        //2.构建SearchSourceBuilder查询对象
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //3.带分析的查询
        QueryBuilder queryBuilder = QueryBuilders.matchQuery("title", name);
        //4.将QueryBuilder对象设置到SearchSourceBuilder中
        //分页
        searchSourceBuilder.from(startIndex);
        searchSourceBuilder.size(pageSize);
        searchSourceBuilder.query(queryBuilder);
        //高亮显示
        //构建HighlightBuilder高亮对象
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //设置要高亮的字段
        highlightBuilder.field("title");
        //设置高亮样式
        highlightBuilder.preTags("<font color='red'>");
        highlightBuilder.postTags("</font>");
        //将高亮对象highlightBuilder设置到searchSourceBuilder中
        searchSourceBuilder.highlighter(highlightBuilder);
        //5.将SearchSourceBuilder对象封装到请求对象SearchRequest中
        searchRequest.source(searchSourceBuilder);
        //6.调用方法进行数据通信
        try {
            SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
            //7.解析输出结果
            SearchHit[] hits = search.getHits().getHits();
            for (SearchHit hit : hits) {
                String jsonString = hit.getSourceAsString();
                TbItem tbItem = JSONObject.parseObject(jsonString, TbItem.class);
                //高亮显示
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                HighlightField title = highlightFields.get("title");
                Text[] fragments = title.getFragments();
                for (Text fragment : fragments) {
                    System.out.println(fragment.toString());
                    tbItem.setTitle(fragment.toString());
                }
                tbItemList.add(tbItem);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tbItemList;
    }

    @Override
    public List<TbItem> getTitle(Long goodsId) {
        return null;
    }
}
