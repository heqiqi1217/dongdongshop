package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbSeckillGoodsMapper;
import com.dongdongshop.mapper.TbSeckillOrderMapper;
import com.dongdongshop.pojo.TbSeckillGoods;
import com.dongdongshop.pojo.TbSeckillGoodsExample;
import com.dongdongshop.service.TbSeckillGoodService;
import com.dongdongshop.util.IdWorker;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbSeckillGoodServiceImpl implements TbSeckillGoodService {

    @Autowired
    private TbSeckillGoodsMapper tbSeckillGoodsMapper;

    @Autowired
    private TbSeckillOrderMapper tbSeckillOrderMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private IdWorker idWorker;

    //查询tb_seckill_goods表
    @Override
    public List<TbSeckillGoods> findGoods() {
        //针对redis的String类型的value进行序列化,只支持String类型和hash类型
        redisTemplate.setValueSerializer(new StringRedisSerializer());

        //查询缓存中是否有需要秒杀的商品
        List<TbSeckillGoods> seckillGoods = redisTemplate.boundHashOps("seckillGoods").values();
        if(seckillGoods == null || seckillGoods.size() <= 0){   //若redis中没有,就去数据库中查
            //查询条件:1.状态为1的; 2.在开始时间和结束时间之间的; 3.剩余库存量大于0的
            TbSeckillGoodsExample tbSeckillGoodsExample = new TbSeckillGoodsExample();
            TbSeckillGoodsExample.Criteria criteria = tbSeckillGoodsExample.createCriteria();
            criteria.andStatusEqualTo("1");
            Date now = new Date();
            criteria.andStartTimeLessThanOrEqualTo(now);
            criteria.andEndTimeGreaterThan(now);
            criteria.andStockCountGreaterThan(0);
            seckillGoods = tbSeckillGoodsMapper.selectByExample(tbSeckillGoodsExample);
        }
        //查经数据库查询到之后,把数据放入到redis中
        for (TbSeckillGoods goods:seckillGoods) {
            //将列表数据放入redis中
            redisTemplate.boundHashOps("seckillGoods").put(goods.getId(),goods);

            //将预减库存单独放入redis中
            redisTemplate.boundValueOps(goods.getId()).set(goods.getStockCount().toString());
        }
        return seckillGoods;
    }

    //跳转到seckill-item页面,抢购页面
    @Override
    public TbSeckillGoods findGoodsById(Long id) {
        return (TbSeckillGoods)redisTemplate.boundHashOps("seckillGoods").get(id);
    }

    //创建订单
    @Override
    public void createOrder(Long goodsId, String username) throws Exception {
        //TODO 先判断当前用户有没有购买过该商品,用登录用户名 + 商品id做key

        //获取redis中的预减库存
        String stockCount = (String)redisTemplate.boundValueOps(goodsId).get();
        //若从redis中查询出来的预减库存为null或者商品数量为0,就返回该商品已售罄
        if(stockCount == null || Long.parseLong(stockCount) == 0){
            throw new Exception("商品已售罄");
        }
        //若从redis中查询出来有该商品,就库存减1,并且更新redis
        //seckillGoods.setStockCount(seckillGoods.getStockCount() - 1); //这三步在多线程时,不能解决并发问题,所以要借助redis的自增(减)命令,要求是数据类型必须是String类型
        //redisTemplate.boundHashOps("seckillGoods").put(seckillGoods.getId(),seckillGoods);

        Long decrement = redisTemplate.boundValueOps(goodsId).decrement();//使用redis的自减,并返回减过的库存
        //根据商品id查询rediis
        TbSeckillGoods seckillGoods = (TbSeckillGoods)redisTemplate.boundHashOps("seckillGoods").get(goodsId);
        //若该商品是最后一件了,那么就更新数据库,并把该商品从redis中删除掉
        if(decrement == 0){
            //若卖完了,就把数据库中该商品的剩余库存量修改为0
            seckillGoods.setStockCount(0);
            tbSeckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
            redisTemplate.boundHashOps("seckillGoods").delete(seckillGoods.getId());
        }

        //TODO 将用户id以及商品id作为key放入缓存,解决同一个用户重复秒杀的问题

        //把seckillGoods通过mq发送出去,异步创建订单
        String str = username + "," + goodsId;
        rocketMQTemplate.convertAndSend("seckill-goods",str);
    }

    //查询订单号
    @Override
    public Long getOrderId(String username) {
        return (Long)redisTemplate.boundHashOps("orderIds").get(username);
    }

    //查询所有秒杀商品
    @Override
    public List<TbSeckillGoods> selectAllGoods() {
        return tbSeckillGoodsMapper.selectByExample(null);
    }

    //往redis中添加
    @Override
    public void addGoodsToRedis(long parseLong) {
        //获取数据库中的数据
        TbSeckillGoods tbSeckillGoods = tbSeckillGoodsMapper.selectByPrimaryKey(parseLong);
        if(tbSeckillGoods.getStockCount() > 0 && tbSeckillGoods.getStatus().equals("1")){
            //添加数据到redis
            redisTemplate.boundHashOps("seckillGoods").put(tbSeckillGoods.getId(),tbSeckillGoods);
            //将预减库存单独放入redis中
            redisTemplate.boundValueOps(tbSeckillGoods.getId()).set(tbSeckillGoods.getStockCount().toString());
        }
    }

    //删除redis中的数据
    @Override
    public void deleteRedisGoods(long parseLong) {
        //删除商品
        redisTemplate.boundHashOps("seckillGoods").delete(parseLong);
        //删除库存
        redisTemplate.delete(parseLong);
    }
}
