package com.dongdongshop.mq;

import com.dongdongshop.mapper.TbSeckillGoodsMapper;
import com.dongdongshop.mapper.TbSeckillOrderMapper;
import com.dongdongshop.pojo.TbSeckillGoods;
import com.dongdongshop.pojo.TbSeckillOrder;
import com.dongdongshop.util.IdWorker;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@RocketMQMessageListener(consumerGroup = "seckill-consumer",topic = "seckill-goods")
public class SeckillGoodsMQ implements RocketMQListener<String>, RocketMQPushConsumerLifecycleListener {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private TbSeckillOrderMapper tbSeckillOrderMapper;

    @Autowired
    private TbSeckillGoodsMapper tbSeckillGoodsMapper;

    @Override
    public void onMessage(String str) {

    }

    @Override
    public void prepareStart(DefaultMQPushConsumer defaultMQPushConsumer) {
        defaultMQPushConsumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                MessageExt messageExt = list.get(0);
                int reconsumeTimes = messageExt.getReconsumeTimes();//重试次数
                try{
                    Boolean mqMessage = redisTemplate.boundHashOps("seckillGoods").hasKey(messageExt.getMsgId());
                    if(!mqMessage){
                        String str = new String(messageExt.getBody());//消息内容
                        String[] split = str.split(",");
                        Long goodsId = Long.parseLong(split[1]);

                        //根据id查询tb_seckill_goods表拿到支付金额和商家id
                        TbSeckillGoods tbSeckillGoods = tbSeckillGoodsMapper.selectByPrimaryKey(goodsId);
                        //创建订单
                        long seckillOrderId = idWorker.nextId();
                        TbSeckillOrder tbSeckillOrder = new TbSeckillOrder();
                        tbSeckillOrder.setId(seckillOrderId);
                        tbSeckillOrder.setCreateTime(new Date());  //创建时间
                        tbSeckillOrder.setMoney(tbSeckillGoods.getCostPrice());  //支付金额
                        tbSeckillOrder.setSeckillId(goodsId); //秒杀商品id
                        tbSeckillOrder.setSellerId(tbSeckillGoods.getSellerId());    //商家id
                        tbSeckillOrder.setStatus("0");  //支付状态
                        tbSeckillOrder.setUserId(split[0]);  //买家
                        tbSeckillOrderMapper.insertSelective(tbSeckillOrder);   //添加订单
                        System.out.println("消费成功");
                        //把订单id放到redis中返回
                        redisTemplate.boundHashOps("orderIds").put(split[0],seckillOrderId);
                    }
                } catch (Exception e){
                    if(reconsumeTimes > 2){
                        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; //消费成功
                    }
                    return ConsumeConcurrentlyStatus.RECONSUME_LATER; //若消费失败,稍后再消费
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; //消费成功
            }
        });
    }
}
