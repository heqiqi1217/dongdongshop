package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbItemCatMapper;
import com.dongdongshop.pojo.TbItemCat;
import com.dongdongshop.service.TbItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbItemCatServiceImpl implements TbItemCatService {

    @Autowired
    private TbItemCatMapper tbItemCatMapper;

    //展示分类表(加查询下级)
    @Override
    public List<TbItemCat> listTbItemCat(Long id) {
        return tbItemCatMapper.listTbItemCat(id);
    }

    //拼接导航栏
    @Override
    public List<TbItemCat> getNavigation(Integer ids) {
        List<TbItemCat> list = new ArrayList<>();
        List<TbItemCat> tbItemCats = get(ids.longValue(), list);
        return tbItemCats;
    }

    //递归
    public List<TbItemCat> get(Long ids,List<TbItemCat> list){
        //根据传过来的id查它的上级parentId
        TbItemCat tbItemCat = tbItemCatMapper.selectByPrimaryKey(ids.longValue());

        //判断返回的对象的parentId是否为0,若不是0,继续拼接
        if(tbItemCat.getParentId() != 0){
            get(tbItemCat.getParentId(),list);
            list.add(tbItemCat);
        }
        return list;
    }

    //增加
    @Override
    public int addTbItemCat(TbItemCat tbItemCat) {
        System.out.println(tbItemCat);
        return tbItemCatMapper.insertSelective(tbItemCat);
    }

    //回显
    @Override
    public TbItemCat selectTbItemCatById(Integer id) {
        return tbItemCatMapper.selectByPrimaryKey(id.longValue());
    }

    //修改
    @Override
    public int updateTbItemCat(TbItemCat tbItemCat) {
        return tbItemCatMapper.updateByPrimaryKeySelective(tbItemCat);
    }


    //批量删除
    @Override
    @Transactional
    public int deleteTbItemCat(Integer[] ids) {
        int i = 0;
        for (Integer id:ids) {
            //删除父级
            i = tbItemCatMapper.deleteByPrimaryKey(id.longValue());

            //删除子级
            tbItemCatMapper.delete(id);
        }
        return i;
    }

    //查询tbItemCat表(三级联动用)
    @Override
    public List<TbItemCat> selectItemCat() {
        return tbItemCatMapper.selectByExample(null);
    }

    //根据模板表的主键id查询出对应的模板id(拼接模板id用)
    @Override
    public TbItemCat getTypeId(Integer id) {
        return tbItemCatMapper.selectByPrimaryKey(id.longValue());
    }

    @Override
    public TbItemCat selectByPrimaryKey(Long category3Id) {
        return tbItemCatMapper.selectByPrimaryKey(category3Id);
    }
}
