package com.dongdongshop.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.mapper.TbSpecificationMapper;
import com.dongdongshop.mapper.TbSpecificationOptionMapper;
import com.dongdongshop.pojo.TbSpecification;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.pojo.TbSpecificationOptionExample;
import com.dongdongshop.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class SpecificationServiceImpl implements SpecificationService {

    @Autowired
    private TbSpecificationMapper tbSpecificationMapper;

    @Autowired
    private TbSpecificationOptionMapper tbSpecificationOptionMapper;

    //查询规格表(模糊查询)
    @Override
    public List<TbSpecification> listSpecification(String spceName) {
        List<TbSpecification> list = tbSpecificationMapper.listSpecification(spceName);
        return list;
    }

    //增加规格表和规格选项表
    @Override
    @Transactional
    public int addSpecification(TbSpecification tbSpecification,String optionListJson) {
        //增加规格表并返回主键
        tbSpecificationMapper.addSpecification(tbSpecification);
        Long id = tbSpecification.getId();
        //JSON字符串转为list集合
        List<TbSpecificationOption> tbSpecificationOptions = JSONObject.parseArray(optionListJson, TbSpecificationOption.class);

        //增加规格选项表
        int i = 0;

        for (TbSpecificationOption list:tbSpecificationOptions) {
             i = tbSpecificationOptionMapper.addTbSpecificationOption(id,list.getOptionName(),list.getOrders());
        }
        return i;
    }

    //回显规格表
    @Override
    public TbSpecification selectSpecificationById(Integer id) {
        return tbSpecificationMapper.selectByPrimaryKey(id.longValue());
    }

    //修改规格表和规格选项表
    @Override
    @Transactional
    public int updateSpecificationById(TbSpecification tbSpecification, String updateOptionListJson) {
        //修改规格表
        tbSpecificationMapper.updateByPrimaryKeySelective(tbSpecification);

        //获取规格选项表中的specId
        TbSpecificationOption tbSpecificationOption = new TbSpecificationOption();
        Long specId = tbSpecification.getId();

        //先删除原来的规格选项和排序,再增加新的
        TbSpecificationOptionExample tsoe = new TbSpecificationOptionExample();
        tsoe.createCriteria().andSpecIdEqualTo(specId);
        tbSpecificationOptionMapper.deleteByExample(tsoe);

        //修改规格选项表
        int i = 0;
        List<TbSpecificationOption> tbSpecificationOptions = JSONObject.parseArray(updateOptionListJson, TbSpecificationOption.class);
        for (TbSpecificationOption list:tbSpecificationOptions) {
            i = tbSpecificationOptionMapper.addTbSpecificationOption(specId,list.getOptionName(),list.getOrders());
        }
        return i;
    }

    //批量删除规格表和规格选项表
    @Override
    @Transactional
    public int deleteSpecificationByIds(Integer[] ids) {
        //删除规格表
        for (Integer id:ids) {
            tbSpecificationMapper.deleteByPrimaryKey(id.longValue());
        }

        //删除规格选项表
        int i = 0;
        TbSpecificationOptionExample tsoe = new TbSpecificationOptionExample();
        for (Integer id:ids) {
            tsoe.createCriteria().andSpecIdEqualTo(id.longValue());
            i = tbSpecificationOptionMapper.deleteByExample(tsoe);
        }
        return i;
    }
}
