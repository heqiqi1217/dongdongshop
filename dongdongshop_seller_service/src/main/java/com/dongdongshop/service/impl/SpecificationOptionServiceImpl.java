package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbSpecificationOptionMapper;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.service.SpecificationOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class SpecificationOptionServiceImpl implements SpecificationOptionService {

    @Autowired
    private TbSpecificationOptionMapper tbSpecificationOptionMapper;

    //回显规格选项名和排序
    @Override
    @Transactional
    public List<TbSpecificationOption> selectOptionNameAndOrdersBySpecid(Integer id) {
        List<TbSpecificationOption> list = tbSpecificationOptionMapper.selectOptionNameAndOrdersBySpecid(id);
        return list;
    }
}
