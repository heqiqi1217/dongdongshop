package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbContentMapper;
import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ContentServiceImpl implements ContentService {

    @Autowired
    private TbContentMapper tbContentMapper;

    //展示广告表
    @Override
    public List<TbContent> selectContent() {
        return tbContentMapper.selectContent();
    }

    //保存
    @Override
    public int addContent(TbContent tbContent) {
        return tbContentMapper.insertSelective(tbContent);
    }

    //批量删除
    @Override
    @Transactional
    public int deleteContentByIds(Integer[] ids) {
        int i = 0;
        for (Integer id:ids) {
            i = tbContentMapper.deleteByPrimaryKey(id.longValue());
        }
        return i;
    }

    //回显广告
    @Override
    public TbContent selectContentByid(Integer id) {
        return tbContentMapper.selectByPrimaryKey(id.longValue());
    }

    //修改
    @Override
    public int updateContentByid(TbContent tbContent) {
        return tbContentMapper.updateByPrimaryKeySelective(tbContent);
    }

    //查询广告表
    @Override
    public List<TbContent> showAll(Long categoryId) {

        return tbContentMapper.showAll(categoryId);
    }
}
