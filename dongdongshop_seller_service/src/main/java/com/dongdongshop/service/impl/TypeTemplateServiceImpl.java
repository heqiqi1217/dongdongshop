package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbTypeTemplateMapper;
import com.dongdongshop.pojo.TbTypeTemplate;
import com.dongdongshop.pojo.TbTypeTemplateExample;
import com.dongdongshop.service.TypeTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@com.alibaba.dubbo.config.annotation.Service
public class TypeTemplateServiceImpl implements TypeTemplateService {

    @Autowired
    private TbTypeTemplateMapper tbTypeTemplateMapper;

    //查询模板表(加模糊查询)
    @Override
    public List<TbTypeTemplate> listTypeTemplates(String name) {
        return tbTypeTemplateMapper.listTypeTemplates(name);
    }

    //增加模板表
    @Override
    @Transactional
    public int addTypeTemplate(TbTypeTemplate tbTypeTemplate) {
        return tbTypeTemplateMapper.insertSelective(tbTypeTemplate);
    }

    //批量删除
    @Override
    @Transactional
    public int deleteTypeTemplate(Integer[] ids) {
        TbTypeTemplateExample ttte = new TbTypeTemplateExample();
        int i = 0;
        for (Integer id:ids) {
            ttte.createCriteria().andIdEqualTo(id.longValue());
            i = tbTypeTemplateMapper.deleteByExample(ttte);
        }
        return i;
    }

    //回显
    @Override
    @Transactional
    public TbTypeTemplate selectTypeTemplateById(Integer id) {
        return tbTypeTemplateMapper.selectByPrimaryKey(id.longValue());
    }

    //修改
    @Override
    @Transactional
    public int updateTypeTemplate(TbTypeTemplate tbTypeTemplate) {
        return tbTypeTemplateMapper.updateByPrimaryKeySelective(tbTypeTemplate);
    }
}
