package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbContentCategoryMapper;
import com.dongdongshop.pojo.TbContentCategory;
import com.dongdongshop.service.ContentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ContentCategoryServiceImpl implements ContentCategoryService {

    @Autowired
    private TbContentCategoryMapper tbContentCategoryMapper;

    //模糊查询广告分类表
    @Override
    public List<TbContentCategory> likeSelect(String name) {
        return tbContentCategoryMapper.likeSelect(name);
    }

    //新建
    @Override
    public int addContentCategory(TbContentCategory tbContentCategory) {
        return tbContentCategoryMapper.insertSelective(tbContentCategory);
    }

    //回显
    @Override
    public TbContentCategory selectById(Long id) {
        return tbContentCategoryMapper.selectByPrimaryKey(id);
    }

    //修改
    @Override
    public int updateContentCategory(TbContentCategory tbContentCategory) {
        return tbContentCategoryMapper.updateByPrimaryKeySelective(tbContentCategory);
    }

    //批量删除
    @Override
    @Transactional
    public int deletecontentCategoryByIds(Integer[] ids) {
        int i = 0;
        for (Integer id:ids) {
            i = tbContentCategoryMapper.deleteByPrimaryKey(id.longValue());
        }
        return i;
    }
}
