package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbBrandMapper;
import com.dongdongshop.pojo.TbBrand;
import com.dongdongshop.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private TbBrandMapper tbBrandMapper;

    //查询品牌表
    @Override
    public List<TbBrand> brandList() {
        return tbBrandMapper.selectByExample(null);
    }

    //增加品牌表
    @Override
    public int addBrand(TbBrand tbBrand) {
        return tbBrandMapper.insertSelective(tbBrand);
    }

    //回显品牌
    @Override
    public TbBrand selectBrandById(Integer id) {
        return tbBrandMapper.selectByPrimaryKey(id.longValue());
    }

    //修改品牌
    @Override
    public int updateBrand(TbBrand tbBrand) {
        return tbBrandMapper.updateByPrimaryKeySelective(tbBrand);
    }

    //批量删除
    @Override
    @Transactional
    public int deleteBrandByIds(Integer[] ids) {
        int i = 0;
        for (Integer id : ids) {
            i = tbBrandMapper.deleteByPrimaryKey(id.longValue());
        }
        return i;
    }

    @Override
    public TbBrand selectByPrimaryKey(Long brandId) {
        return tbBrandMapper.selectByPrimaryKey(brandId);
    }
}

