package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbSellerMapper;
import com.dongdongshop.mapper.TbSpecificationOptionMapper;
import com.dongdongshop.mapper.TbTypeTemplateMapper;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.pojo.TbTypeTemplate;
import com.dongdongshop.service.TbSellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbSellerServiceImpl implements TbSellerService {

    @Autowired
    private TbSellerMapper tbSellerMapper;

    @Autowired
    private TbTypeTemplateMapper tbTypeTemplateMapper;

    @Autowired
    private TbSpecificationOptionMapper tbSpecificationOptionMapper;

    //先查询出商家表中的所有商家
    @Override
    public List<TbSeller> selectAllTbSeller() {
        return tbSellerMapper.selectByExample(null);
    }

    //注册新商家
    @Override
    public void register(TbSeller tbSeller) {
        tbSellerMapper.insertSelective(tbSeller);
    }

    //登录
    @Override
    public TbSeller login(String username) {
        return tbSellerMapper.login(username);
    }

    //商家管理(加模糊查询)
    @Override
    public List<TbSeller> listSeller(String name,String nickName,String status) {
        return tbSellerMapper.listSeller(name,nickName,status);
    }

    //详情
    @Override
    public TbSeller listSellerBySellerId(String sellerId) {
        return tbSellerMapper.listSellerBySellerId(sellerId);
    }

    //审核通过
    @Override
    public int approvedBySellerId(String sellerId) {
        return tbSellerMapper.approvedBySellerId(sellerId);
    }

    //审核未通过
    @Override
    public int notApprovedBySellerId(String sellerId) {
        return tbSellerMapper.notApprovedBySellerId(sellerId);
    }

    //关闭商家
    @Override
    public int closeBySellerId(String sellerId) {
        return tbSellerMapper.closeBySellerId(sellerId);
    }

    //根据模板表的模板id查询规格表
    @Override
    public TbTypeTemplate selectTypeTemplate(Integer id) {
        return tbTypeTemplateMapper.selectByPrimaryKey(id.longValue());
    }

    //根据规格表的id查询规格选项表
    @Override
    public List<TbSpecificationOption> selectSpecificationOption(Long id) {
        return tbSpecificationOptionMapper.selectSpecificationOption(id);
    }
}
