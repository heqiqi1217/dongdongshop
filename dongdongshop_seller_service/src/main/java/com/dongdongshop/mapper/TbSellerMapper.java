package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.pojo.TbSellerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbSellerMapper {
    int countByExample(TbSellerExample example);

    int deleteByExample(TbSellerExample example);

    int deleteByPrimaryKey(String sellerId);

    int insert(TbSeller record);

    int insertSelective(TbSeller record);

    List<TbSeller> selectByExample(TbSellerExample example);

    TbSeller selectByPrimaryKey(String sellerId);

    int updateByExampleSelective(@Param("record") TbSeller record, @Param("example") TbSellerExample example);

    int updateByExample(@Param("record") TbSeller record, @Param("example") TbSellerExample example);

    int updateByPrimaryKeySelective(TbSeller record);

    int updateByPrimaryKey(TbSeller record);

    //登录
    TbSeller login(String username);

    //详情
    TbSeller listSellerBySellerId(String sellerId);

    //审核通过
    int approvedBySellerId(String sellerId);

    //审核未通过
    int notApprovedBySellerId(String sellerId);

    //关闭商家
    int closeBySellerId(String sellerId);

    //商家管理(加模糊查询)
    List<TbSeller> listSeller(@Param("name")String name, @Param("nickName")String nickName, @Param("status")String status);
}