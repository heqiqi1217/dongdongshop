package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.pojo.TbSpecificationOptionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbSpecificationOptionMapper {
    int countByExample(TbSpecificationOptionExample example);

    int deleteByExample(TbSpecificationOptionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbSpecificationOption record);

    int insertSelective(TbSpecificationOption record);

    List<TbSpecificationOption> selectByExample(TbSpecificationOptionExample example);

    TbSpecificationOption selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbSpecificationOption record, @Param("example") TbSpecificationOptionExample example);

    int updateByExample(@Param("record") TbSpecificationOption record, @Param("example") TbSpecificationOptionExample example);

    int updateByPrimaryKeySelective(TbSpecificationOption record);

    int updateByPrimaryKey(TbSpecificationOption record);

    //增加规格选项表
    int addTbSpecificationOption(@Param("id") Long id, @Param("optionName") String optionName, @Param("orders") Integer orders);

    //回显规格选项名和排序
    List<TbSpecificationOption> selectOptionNameAndOrdersBySpecid(Integer id);

    //根据规格表的id查询规格选项表
    List<TbSpecificationOption> selectSpecificationOption(Long id);
}