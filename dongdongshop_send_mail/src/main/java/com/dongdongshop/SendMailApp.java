package com.dongdongshop;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/13 11:12
 * @Version
 **/
@SpringBootApplication
@EnableDubbo
public class SendMailApp {

    public static void main(String[] args) {
        SpringApplication.run(SendMailApp.class,args);
    }
}
