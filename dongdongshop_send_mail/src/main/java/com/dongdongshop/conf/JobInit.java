package com.dongdongshop.conf;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.job.Job2;
import com.dongdongshop.job.Job3;
import com.dongdongshop.job.MailJob;
import com.dongdongshop.pojo.TbSeckillGoods;
import com.dongdongshop.service.TbSeckillGoodService;
import com.dongdongshop.util.QuartzCronDateUtils;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Component
public class JobInit {

    @Autowired
    private Scheduler scheduler;

    @Reference
    private TbSeckillGoodService tbSeckillGoodService;

    //初始化
    @PostConstruct
    public void initjob() throws SchedulerException {
        //定义任务调度实例与Job绑定
        JobDetail detail = JobBuilder.newJob(MailJob.class)
                .withIdentity("job_one", "job") //设置job名和组名
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()//定义任务何时执行，何时结束,触发器
                .withIdentity("trigger_one", "trigger") //设置Trigger名和组名
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ? "))//设置调度器
                .build();
        scheduler.scheduleJob(detail, trigger);
    }

    //初始化
    @PostConstruct
    public void initjob1() throws SchedulerException {
        List<TbSeckillGoods> list = tbSeckillGoodService.selectAllGoods();
        for (TbSeckillGoods tbSeckillGoods : list) {
            //秒杀开始时间
            Date startTime = tbSeckillGoods.getStartTime();
            String cron = QuartzCronDateUtils.getCron(startTime);
            //定义任务调度实例与Job绑定
            JobDetail detail = JobBuilder.newJob(Job2.class)
                    .withIdentity(tbSeckillGoods.getId() + "j", "job") //设置job名和组名
                    .usingJobData("tbSeckillGoods",tbSeckillGoods.getId().toString())
                    .build();

            Trigger trigger = TriggerBuilder.newTrigger()//定义任务何时执行，何时结束,触发器
                    .withIdentity(tbSeckillGoods.getId()+"t", "trigger") //设置Trigger名和组名
                    .startNow()
                    .withSchedule(CronScheduleBuilder.cronSchedule(cron))//设置调度器
                    .build();
            scheduler.scheduleJob(detail, trigger);

            //秒杀结束时间
            Date endTime = tbSeckillGoods.getEndTime();
            String cron1 = QuartzCronDateUtils.getCron(endTime);
            //定义任务调度实例与Job绑定
            JobDetail detail1 = JobBuilder.newJob(Job3.class)
                    .withIdentity(tbSeckillGoods.getId() + "a", "job") //设置job名和组名
                    .usingJobData("tbSeckillGoods",tbSeckillGoods.getId().toString())
                    .build();

            Trigger trigger1 = TriggerBuilder.newTrigger()//定义任务何时执行，何时结束,触发器
                    .withIdentity(tbSeckillGoods.getId()+"b", "trigger") //设置Trigger名和组名
                    .startNow()
                    .withSchedule(CronScheduleBuilder.cronSchedule(cron1))//设置调度器
                    .build();
            scheduler.scheduleJob(detail1, trigger1);
        }
    }
}
