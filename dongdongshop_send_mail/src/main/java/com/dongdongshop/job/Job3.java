package com.dongdongshop.job;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.service.TbSeckillGoodService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/13 19:05
 * @Version
 **/
public class Job3 implements Job {

    @Reference
    private TbSeckillGoodService tbSeckillGoodService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String tbSeckillGoodsId = (String)jobExecutionContext.getJobDetail().getJobDataMap().get("tbSeckillGoods");
        //删除redis中的数据
        tbSeckillGoodService.deleteRedisGoods(Long.parseLong(tbSeckillGoodsId));
    }
}
