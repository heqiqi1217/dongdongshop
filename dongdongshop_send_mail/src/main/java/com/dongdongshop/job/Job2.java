package com.dongdongshop.job;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.service.TbSeckillGoodService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/13 12:14
 * @Version
 **/
public class Job2 implements Job {

    @Reference
    private TbSeckillGoodService tbSeckillGoodService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String tbSeckillGoodsId = (String)jobExecutionContext.getJobDetail().getJobDataMap().get("tbSeckillGoods");
        //往redis里增加
        tbSeckillGoodService.addGoodsToRedis(Long.parseLong(tbSeckillGoodsId));
    }
}
