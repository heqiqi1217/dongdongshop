package com.dongdongshop;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/10 10:34
 * @Version
 **/
@SpringBootApplication
@EnableDubbo
public class CommentServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(CommentServiceApp.class,args);
    }
}
