package com.dongdongshop.service.impl;

import com.dongdongshop.pojo.Comment;
import com.dongdongshop.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/10 13:41
 * @Version
 **/
@Service
@com.alibaba.dubbo.config.annotation.Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private MongoTemplate mongoTemplate;

    //增加评论
    @Override
    public Comment addComment(Comment comment) {

        return mongoTemplate.save(comment);
    }

    //查询评价
    @Override
    public List<Comment> getComment(Long goodsId,Integer type) {
        //条件查询
        Query query = new Query(Criteria.where("goodsId").is(goodsId)); //商品id
        if(type == 3){
            query.addCriteria(Criteria.where("type").is("好评"));   //评论类型
        }else if(type == 1){
            query.addCriteria(Criteria.where("type").is("中评"));   //评论类型
        }else if(type == 2){
            query.addCriteria(Criteria.where("type").is("差评"));   //评论类型
        }
        //查询总条数
        //Long count = mongoTemplate.count(query, Comment.class);
        //分页功能
        //query.skip((pageNum-1) * pageSize).limit(pageSize);
        //分页查询数据
        List<Comment> comments = mongoTemplate.find(query,Comment.class);
        return comments;
    }
}
