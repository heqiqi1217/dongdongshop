package com.dongdongshop;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Deacription TODO
 * @Author 93753
 * @Date 2021/10/5 14:31
 * @Version
 **/
@SpringBootApplication
@EnableDubbo
@MapperScan("com.dongdongshop.mapper")
public class SellerServiceApp1 {
    public static void main(String[] args) {
        SpringApplication.run(SellerServiceApp1.class,args);
    }
}
