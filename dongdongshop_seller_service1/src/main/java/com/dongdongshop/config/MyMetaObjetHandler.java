package com.dongdongshop.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;


/*
* 自动创建时间和自动更新时间的配置类
* */
@Component
public class MyMetaObjetHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {

        /*
        * 第一个参数是表字段
        * 第二个参数是需要修改的字段值
        * 第三个参数是给数据进行处理
        * */
        this.setFieldValByName("createTime",new Date(),metaObject);
        this.setFieldValByName("updateTime",new Date(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime",new Date(),metaObject);
    }
}
