package com.dongdongshop.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/*
* 分页的配置类
* */
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class MybatisPlusPageConfig {

    /*
    * 分页的插件
    * */
    @Bean
    public PaginationInterceptor paginationInterceptorn(){
       return new PaginationInterceptor();
    }
}
