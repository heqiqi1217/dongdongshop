package com.dongdongshop.service.impl;

import com.dongdongshop.pojo.TbBrand;
import com.dongdongshop.service.BrandService;
import com.dongdongshop.service.TbBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TbrandServiceImpl implements BrandService {

    @Autowired
    private TbBrandService tbBrandService;

    //查询品牌表
    @Override
    public List<TbBrand> brandList() {
        return tbBrandService.selectByExample();
    }

    //增加品牌表
    @Override
    public int addBrand(TbBrand tbBrand) {
        return tbBrandService.insertSelective(tbBrand);
    }

    //回显品牌
    @Override
    public TbBrand selectBrandById(Integer id) {
        return tbBrandService.selectByPrimaryKey(id.longValue());
    }

    //修改品牌
    @Override
    public int updateBrand(TbBrand tbBrand) {
        return tbBrandService.updateByPrimaryKeySelective(tbBrand);
    }

    //批量删除
    @Override
    @Transactional
    public int deleteBrandByIds(Integer[] ids) {
       /* int i = 0;
        for (Integer id : ids) {
            i = tbBrandService.deleteByPrimaryKey(id.longValue());
        }*/
        int i = tbBrandService.deleteByPrimaryKey(ids);
        return i;
    }

    @Override
    public TbBrand selectByPrimaryKey(Long brandId) {
        return tbBrandService.selectByPrimaryKey(brandId);
    }
}

