package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbUserScoreAccount;
import com.dongdongshop.pojo.TbUserScoreAccountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbUserScoreAccountMapper {
    int countByExample(TbUserScoreAccountExample example);

    int deleteByExample(TbUserScoreAccountExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbUserScoreAccount record);

    int insertSelective(TbUserScoreAccount record);

    List<TbUserScoreAccount> selectByExample(TbUserScoreAccountExample example);

    TbUserScoreAccount selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbUserScoreAccount record, @Param("example") TbUserScoreAccountExample example);

    int updateByExample(@Param("record") TbUserScoreAccount record, @Param("example") TbUserScoreAccountExample example);

    int updateByPrimaryKeySelective(TbUserScoreAccount record);

    int updateByPrimaryKey(TbUserScoreAccount record);

    //根据userId查询积分表,若有该用户,就直接在原来基础上加
    TbUserScoreAccount selectScoreAccountByUserId(String userId);
}