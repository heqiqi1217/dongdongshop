package com.dongdongshop.mq;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbUserScoreAccount;
import com.dongdongshop.service.ScoreAccountService;
import com.dongdongshop.service.TbOrderService;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@RocketMQMessageListener(topic="tx-topic",consumerGroup = "tx-group")
public class RocketMQConsumer implements RocketMQListener<String>, RocketMQPushConsumerLifecycleListener {

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbOrderService tbOrderService;

    @Reference
    private ScoreAccountService scoreAccountService;

    @Override
    public void onMessage(String str) {

    }


    @Override
    public void prepareStart(DefaultMQPushConsumer defaultMQPushConsumer) {
        defaultMQPushConsumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                MessageExt messageExt = list.get(0);
                int reconsumeTimes = messageExt.getReconsumeTimes();//重试次数
                try{
                    Boolean mqMessage = redisTemplate.boundHashOps("mqMessage").hasKey(messageExt.getMsgId());
                    if(!mqMessage){
                        String str = new String(messageExt.getBody());//消息内容
                        TbOrder tbOrder = JSONObject.parseObject(str, TbOrder.class);
                        String outTradeNo = tbOrder.getOutTradeNo();
                        //查询order表拿到登录用户
                        TbOrder tbOrder1 = tbOrderService.selectUserIdByOutTradeNo(outTradeNo);
                        Long totalScore = 1000l;  //tb_user_score_account表的用户总积分total_score
                        Long expendScore = 100l;  //tb_user_score_account表的用户总消耗积分expend_score
                        Date createTime = new Date();   //tb_user_score_account表的创建时间create-time
                        Date updateTime = new Date();  //tb_user_score_account表的更新时间update_time

                        //根据userId查询积分表,若有该用户,就直接在原来基础上加
                        TbUserScoreAccount tbUserScoreAccount = scoreAccountService.selectScoreAccountByUserId(tbOrder1.getUserId());
                        if(tbUserScoreAccount == null){  //说明积分表中没有该登录用户
                            TbUserScoreAccount tbUserScoreAccount1 = new TbUserScoreAccount();
                            tbUserScoreAccount1.setUserId(tbOrder1.getUserId());
                            tbUserScoreAccount1.setTotalScore(totalScore);
                            tbUserScoreAccount1.setExpendScore(expendScore);
                            tbUserScoreAccount1.setCreateTime(createTime);
                            tbUserScoreAccount1.setUpdateTime(updateTime);
                            //添加tb_user_score_account表
                            scoreAccountService.add(tbUserScoreAccount1);
                        }else {   //说明积分表中有该登录用户,修改该登录用户的积分值和更新时间
                            tbUserScoreAccount.setTotalScore(tbUserScoreAccount.getTotalScore() + totalScore);
                            tbUserScoreAccount.setUpdateTime(new Date());

                            //修改tb_user_score_account表
                            scoreAccountService.update(tbUserScoreAccount);
                        }
                        System.out.println("消费成功");
                    }
                } catch (Exception e){
                    if(reconsumeTimes > 2){
                        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; //消费成功
                    }
                    return ConsumeConcurrentlyStatus.RECONSUME_LATER; //若消费失败,稍后再消费
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; //消费成功
            }
        });
    }
}
