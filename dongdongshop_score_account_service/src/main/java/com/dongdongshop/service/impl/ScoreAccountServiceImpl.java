package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbUserScoreAccountMapper;
import com.dongdongshop.pojo.TbUserScoreAccount;
import com.dongdongshop.service.ScoreAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ScoreAccountServiceImpl implements ScoreAccountService {

    @Autowired
    private TbUserScoreAccountMapper tbUserScoreAccountMapper;

    //添加tb_user_score_account表
    @Override
    public void add(TbUserScoreAccount tbUserScoreAccount) {
        tbUserScoreAccountMapper.insertSelective(tbUserScoreAccount);
    }

    //根据userId查询积分表,若有该用户,就直接在原来基础上加
    @Override
    public TbUserScoreAccount selectScoreAccountByUserId(String userId) {
        return tbUserScoreAccountMapper.selectScoreAccountByUserId(userId);
    }

    //修改tb_user_score_account表
    @Override
    public void update(TbUserScoreAccount tbUserScoreAccount) {
        tbUserScoreAccountMapper.updateByPrimaryKeySelective(tbUserScoreAccount);
    }
}
