package com.dongdongshop;

import com.dongdongshop.pojo.TbGoods;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FreemarkerTest {
    public static void main(String[] args) throws Exception {
//      第一步：创建一个 Configuration 对象，直接 new 一个对象。构造方法的参数就是 freemarker的版本号。
        Configuration configuration = new Configuration(Configuration.getVersion());
//      第二步：设置模板文件所在的路径。
        configuration.setDirectoryForTemplateLoading(new File("E:\\idea\\code\\dongdongshop_parent\\dongdongshop_itemPage_service\\src\\main\\resources"));
//      第三步：设置模板文件使用的字符集。一般就是 utf-8.
        configuration.setDefaultEncoding("utf-8");
//      第四步：加载一个模板，创建一个模板对象。
        Template template = configuration.getTemplate("test.ftl");
//      第五步：创建一个模板使用的数据集，可以是 pojo 也可以是 map。一般是 Map。
        Map map = new HashMap();
        map.put("flag",false);
        map.put("num",1);

        TbGoods tbGoods = new TbGoods();
        tbGoods.setGoodsName("华为");
        tbGoods.setPrice(new BigDecimal(5000));

        TbGoods tbGoods1 = new TbGoods();
        tbGoods1.setGoodsName("小米");
        tbGoods1.setPrice(new BigDecimal(1000));

        List<TbGoods> tbGoodsList = new ArrayList<>();
        tbGoodsList.add(tbGoods);
        tbGoodsList.add(tbGoods1);
        map.put("tbGoodsList",tbGoodsList);
//      第六步：创建一个 Writer 对象，一般创建一 FileWriter 对象，指定生成的文件名。
        FileWriter writer = new FileWriter("E:\\freemarker\\test.html");
//      第七步：调用模板对象的 process 方法输出文件。
        template.process(map,writer);
//      第八步：关闭流
        writer.close();
    }
}
