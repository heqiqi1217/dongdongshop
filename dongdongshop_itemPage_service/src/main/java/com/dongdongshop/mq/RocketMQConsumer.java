package com.dongdongshop.mq;

import com.dongdongshop.service.FreemarkerService;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@RocketMQMessageListener(topic="id",consumerGroup = "myConsumer-group")
public class RocketMQConsumer implements RocketMQListener<Long> , RocketMQPushConsumerLifecycleListener {

    @Autowired
    private FreemarkerService freemarkerService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void onMessage(Long goodsId) {
        //生成静态页面
        //System.out.println("接收消息成功"+ goodsId);
        //freemarkerService.createPage(goodsId);
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer defaultMQPushConsumer) {
        defaultMQPushConsumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                        MessageExt messageExt = list.get(0);
                        int reconsumeTimes = messageExt.getReconsumeTimes();//重试次数
                        try{
                            Boolean mqMessage = redisTemplate.boundHashOps("mqMessage").hasKey(messageExt.getMsgId());
                            if(!mqMessage){
                              String str = new String(messageExt.getBody());//消息内容
                              freemarkerService.createPage(Long.parseLong(str));  //消费消息
                              System.out.println("消费成功");
                            }
                        } catch (Exception e){
                            if(reconsumeTimes > 2){
                                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; //消费成功
                            }
                            return ConsumeConcurrentlyStatus.RECONSUME_LATER; //若消费失败,稍后再消费
                        }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; //消费成功
            }
        });
    }
}
