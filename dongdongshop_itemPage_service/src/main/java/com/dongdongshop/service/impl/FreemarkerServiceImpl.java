package com.dongdongshop.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbGoodsDesc;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.service.FreemarkerService;
import com.dongdongshop.service.GoodDescService;
import com.dongdongshop.service.GoodService;
import com.dongdongshop.service.TbItemService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FreemarkerServiceImpl implements FreemarkerService {

    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @Reference
    private GoodService goodService;

    @Reference
    private GoodDescService goodDescService;

    @Reference
    private TbItemService tbItemService;

    @Override
    public void createPage(Long goodsId) {
        FileWriter writer = null;
        //第一步：创建一个 Configuration 对象，直接 new 一个对象。构造方法的参数就是 freemarker的版本号。
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
        //第四步：加载一个模板，创建一个模板对象。
        try {
            Template template = configuration.getTemplate("item.ftl");
            //第五步：创建一个模板使用的数据集，可以是 pojo 也可以是 map。一般是 Map。
            Map map = new HashMap();
            //第六步：创建一个 Writer 对象，一般创建 FileWriter 对象，指定生成的文件名。
                writer = new FileWriter("E:\\freemarker\\"+ goodsId +".html");
            //数据填充,根据goodsId查询商品信息,商品详情信息和sku表
                TbGoods tbGoods = goodService.selectGoodsByGoodsId(goodsId);
                map.put("tbGoods",tbGoods);

                TbGoodsDesc tbGoodsDesc = goodDescService.selectGoodsListByGoodsId(goodsId);
                map.put("tbGoodsDesc",tbGoodsDesc);

                List<TbItem> tbItemList = tbItemService.selectItemListByGoodsId(goodsId);
                map.put("tbItemList",tbItemList);
            //第七步：调用模板对象的 process 方法输出文件。
            template.process(map,writer);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if(writer != null){
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
