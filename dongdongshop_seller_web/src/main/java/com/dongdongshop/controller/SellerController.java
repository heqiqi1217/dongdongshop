package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.expection.LoginException;
import com.dongdongshop.expection.LoginException2;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.GoodService;
import com.dongdongshop.service.TbItemCatService;
import com.dongdongshop.service.TbSellerService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Controller
@RequestMapping("sellerController")
public class SellerController {

    @Reference
    private TbSellerService tbSellerService;

    @Reference
    private TbItemCatService tbItemCatService;

    @Reference
    private GoodService goodService;

    //登录
    @RequestMapping("login")
    @ResponseBody
    public Result login(String sellerId,String password) {
        Map<String,Object> map = new HashMap<>();
        //获取subject
        Subject subject = SecurityUtils.getSubject();
        //把pname和pwd封装成token对象
        UsernamePasswordToken token = new UsernamePasswordToken(sellerId,password);
        //调用subject的login方法
        try{
            subject.login(token);
        }catch (UnknownAccountException e){
            map.put("message","用户名不存在");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }catch (IncorrectCredentialsException e){
            map.put("message","密码不正确");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }catch (LoginException e){
            map.put("message","未审核");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }catch (LoginException2 e){
            map.put("message","审核不通过");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }
       map.put("message","登录成功");
       return Result.bulid(ConstantEnum.SUCCESS).setResult(map);
    }

    //转到新增商品页面
    @RequestMapping("goodsEdit")
    public String goodsEdit(){
        return "admin/goods_edit";
    }

    //转到商品管理页面
    @RequestMapping("back")
    public String back(){
        return "admin/goods";
    }

    //查询tbItemCat表
    @RequestMapping("selectItemCat")
    @ResponseBody
    public Result selectItemCat(){
        List<TbItemCat> list = tbItemCatService.selectItemCat();
        return Result.bulid(ConstantEnum.SUCCESS).setResult(list);
    }

    //根据模板表的主键id查询出对应的模板id
    @RequestMapping("getTypeId")
    @ResponseBody
    public Result getTypeId(Integer id){
        TbItemCat tbItemCat = tbItemCatService.getTypeId(id);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(tbItemCat);
    }


    //根据模板表的模板id查询规格表,返回品牌
    @RequestMapping("selectTypeTemplate")
    @ResponseBody
    public Result selectTypeTemplate(Integer id){
       TbTypeTemplate tbTypeTemplate = tbSellerService.selectTypeTemplate(id);
       return Result.bulid(ConstantEnum.SUCCESS).setResult(tbTypeTemplate);
    }

    //根据模板表的模板id查询规格表,返回规格与规格选项
    @RequestMapping("selectTypeTemplate1")
    @ResponseBody
    public Result selectTypeTemplate1(Integer id){
        //根据模板id获取模板对象
        TbTypeTemplate tbTypeTemplate = tbSellerService.selectTypeTemplate(id);

        //将模板中的specIds转为集合
        List<TbSpecification> specificationList = JSONObject.parseArray(tbTypeTemplate.getSpecIds(), TbSpecification.class);
        List<SpecVO> specVOList = new ArrayList<>();

        for (TbSpecification specification:specificationList) {
            //根据规格表的id查询规格选项表
            List<TbSpecificationOption> specificationOptionList = tbSellerService.selectSpecificationOption(specification.getId());

            SpecVO specVO = new SpecVO();
            specVO.setTbSpecification(specification);
            specVO.setList(specificationOptionList);
            specVOList.add(specVO);
        }
        return Result.bulid(ConstantEnum.SUCCESS).setResult(specVOList);
    }

    //多文件上传
    @RequestMapping ("uploadFile")
    @ResponseBody
    public List<String> upLoadFiles(@RequestParam(value = "myFile") MultipartFile[] myFile) {

        List<String> list = new ArrayList<>();

        // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
        String endpoint = "oss-cn-beijing.aliyuncs.com";
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = "LTAI5tKqSkBxTE5BbYcfHkMg";
        String accessKeySecret = "SDozOgebvZBF5sKF1yupJhNi39Rdzs";
        String backetName = "dongdongshop-hqq";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 填写字符串。
        //String content = "Hello OSS";

        // 创建PutObjectRequest对象。
        PutObjectRequest putObjectRequest = null;

        for (MultipartFile mu : myFile) {
            try {
                InputStream inputStream = mu.getInputStream();
                System.out.println("name" + mu.getOriginalFilename());
                String fileName ="test/" + UUID.randomUUID() + mu.getOriginalFilename();

                // 依次填写Bucket名称（例如examplebucket）和Object完整路径（例如exampledir/exampleobject.txt）。Object完整路径中不能包含Bucket名称。
                putObjectRequest =
                        new PutObjectRequest(backetName,fileName,inputStream);
                list.add("https://" + backetName + "." + endpoint + "/" + fileName);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
        // ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
        // metadata.setObjectAcl(CannedAccessControlList.Private);
        // putObjectRequest.setMetadata(metadata);

        // 上传字符串。
        ossClient.putObject(putObjectRequest);
        // 关闭OSSClient.
        ossClient.shutdown();
        return list;
    }

    //保存
    @RequestMapping("save")
    @ResponseBody
    public Result save(TbGoods tbGoods, TbGoodsDesc tbGoodsDesc,String listJson,String title,String listSpecJson){
        TbSeller tbSeller = (TbSeller)SecurityUtils.getSubject().getPrincipal();
        tbGoods.setSellerId(tbSeller.getSellerId());   //商家id,还需要再获取商家名称(name)
        String name = tbSeller.getName();//商家名称

        int i = goodService.save(tbGoods,tbGoodsDesc,listJson,name,title,listSpecJson);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }

    //转到商品管理页面
    @RequestMapping("goods")
    public String goods(){
        return "admin/goods";
    }

    //查询商品表goods(加分页)
    @RequestMapping("selectGoods")
    @ResponseBody
    public Result selectGoods(String auditStatus,String goodsName,@RequestParam(defaultValue = "1") Integer pageNum,@RequestParam(defaultValue = "10") Integer pageSize){
        Page<List<TbGoods>> goodsList = goodService.listGoods(auditStatus,goodsName,pageNum,pageSize);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(goodsList);
    }

    //批量删除
    @RequestMapping("deleteGoodsByIds")
    @ResponseBody
    public Result deleteGoodsByIds(Long[] ids){
        int i = goodService.deleteGoodsByIds(ids);
        return Result.bulid(ConstantEnum.SUCCESS).setResult(i);
    }
}
