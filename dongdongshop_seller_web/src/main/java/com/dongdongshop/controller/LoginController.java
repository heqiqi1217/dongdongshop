package com.dongdongshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    //跳到登录页面
    @RequestMapping(path = {"login","/"})
    public String login(){
        return "shoplogin";
    }

    //跳转到index页面
    @RequestMapping("index")
    public String index(){
        return "admin/index";
    }
}
