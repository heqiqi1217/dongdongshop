package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.service.TbSellerService;
import com.dongdongshop.util.ShiroUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("registerController")
public class RegisterController {

    @Reference
    private TbSellerService tbSellerService;

    //转到注册页面
    @RequestMapping("toRegister")
    public String toRegister(){
        return "seller/register";
    }

    //注册
    @RequestMapping("registerSeller")
    @ResponseBody
    public Result register(TbSeller tbSeller){
        //先查询出商家表中的所有商家
        List<TbSeller> tbSellerlist = tbSellerService.selectAllTbSeller();
        Map<String,Object> map = new HashMap<>();
        for (int i = 0; i < tbSellerlist.size(); i++) {
            TbSeller tbSeller1 = tbSellerlist.get(i);
            if (tbSeller1.getSellerId().equals(tbSeller.getSellerId())) {
                map.put("message","商家名已存在");
                return Result.bulid(ConstantEnum.ERROR).setResult(map);
            }
        }
        if (tbSeller.getNickName() == null || tbSeller.getNickName().equals("")) {
            map.put("message","店铺名不能为空");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }
        if (tbSeller.getPassword() == null || tbSeller.getPassword().equals("")) {
            map.put("message","密码不能为空");
            return Result.bulid(ConstantEnum.ERROR).setResult(map);
        }
        //密码加密
        String salt = "123";
        String md5 = ShiroUtils.encryptPassword("MD5", tbSeller.getPassword(), salt, 3);//MD5三次加密之后的密码
        tbSeller.setPassword(md5);

        //注册新商家
        tbSellerService.register(tbSeller);
        return Result.bulid(ConstantEnum.REGISTER_SUCCESS).setResult(map);
    }
}
