package com.dongdongshop.shiro;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.expection.LoginException;
import com.dongdongshop.expection.LoginException2;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.service.TbSellerService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

public class Realm extends AuthorizingRealm {

    @Reference
    private TbSellerService tbSellerService;

    //授权逻辑的方法
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    //认证逻辑的方法
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken)authenticationToken;
        TbSeller tbSeller = tbSellerService.login(token.getUsername());

        //用户名不存在
        if(tbSeller == null){
            return null;
        }

        if(tbSeller.getStatus().equals("0")){
            throw new LoginException("未审核");
        }

        if(tbSeller.getStatus().equals("2")){
            throw new LoginException2("审核不通过");
        }

        //第一个参数是登录成功之后需要放到session中的对象,第二个参数是数据库查询出来的密码,第三个参数是盐,第四个是用户的名字(账号)
        return new SimpleAuthenticationInfo(tbSeller,tbSeller.getPassword(),ByteSource.Util.bytes("123"),tbSeller.getNickName());  //校验密码
    }
}
