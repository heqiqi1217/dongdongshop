package com.dongdongshop.expection;

import org.apache.shiro.authc.AuthenticationException;

public class LoginException2 extends AuthenticationException {

    public LoginException2() {
    }

    public LoginException2(String message) {
        super(message);
    }

    public LoginException2(Throwable cause) {
        super(cause);
    }

    public LoginException2(String message, Throwable cause) {
        super(message, cause);
    }
}
