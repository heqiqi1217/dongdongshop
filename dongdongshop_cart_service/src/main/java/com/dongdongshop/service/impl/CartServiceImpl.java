package com.dongdongshop.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.CartService;
import com.dongdongshop.service.TbItemService;
import com.dongdongshop.service.TbOrderItemService;
import com.dongdongshop.service.TbOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class CartServiceImpl implements CartService {

    @Reference
    private TbItemService tbItemService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbOrderService tbOrderService;

    @Reference
    private TbOrderItemService tbOrderItemService;

    //调用购物车接口,添加购物车数据
    @Override
    public List<Cart> addCart(List<Cart> cartList, Long itemId, Integer num) {
        //1.根据itemId查询item表中的商品
        System.out.println(itemId);
        System.out.println(num);
        TbItem tbItem = tbItemService.selectItemById(itemId);
        //2.第一步执行结束后,取出返回的item表中的商家id
        String sellerId = tbItem.getSellerId();
        //3.根据商家的id来判断原购物车里有没有该商家
        Cart cart = findCartBySellerId(cartList, sellerId);
        if(cart == null){  //4.若原购物车里没有该商家
            //4.1.创建一个cart(购物车)对象
            cart = new Cart();
            //将商家的id放进去
            cart.setSellerId(sellerId);
            //4.2.将要添加的商品放到cart对象中的orderitemList(明细列表)中
            List<TbOrderItem> orderItemList = new ArrayList<>();
            TbOrderItem tbOrderItem = new TbOrderItem();
            /*tbOrderItem.setTitle(tbItem.getTitle());//标题
            tbOrderItem.setItemId(tbItem.getId());
            tbOrderItem.setGoodsId(tbItem.getGoodsId());
            tbOrderItem.setNum(num);//数量
            tbOrderItem.setPrice(tbItem.getPrice());  //单价
            tbOrderItem.setSellerId(tbItem.getSellerId());
            tbOrderItem.setTotalFee(new BigDecimal(tbItem.getPrice().doubleValue() * num));  //小计 单价 * 数量*/
            fengzhuang(tbOrderItem,tbItem,num);
            //把tbOrderItem放到orderItemList中(放到明细列表中)
            orderItemList.add(tbOrderItem);
            //再把orderItemList放到cart中去(放到购物车对象中)
            cart.setOrderItemList(orderItemList);
            //再把cart放到cartList中(放到购物车中)
            cartList.add(cart);
        }else {  //5.若有该商家
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            TbOrderItem tbOrderItem = findOrderItemByItemId(orderItemList, itemId);
            //5.1.判断该商家的数据里(明细)有没有该商品
            if(tbOrderItem == null){  //没有
                //5.1.1 创建明细对象,再往原购物车明细集合中追加一条
                tbOrderItem = new TbOrderItem();
                /*tbOrderItem.setTitle(tbItem.getTitle());//标题
                tbOrderItem.setItemId(tbItem.getId());
                tbOrderItem.setGoodsId(tbItem.getGoodsId());
                tbOrderItem.setNum(num);//数量
                tbOrderItem.setPrice(tbItem.getPrice());  //单价
                tbOrderItem.setSellerId(tbItem.getSellerId());
                tbOrderItem.setTotalFee(new BigDecimal(tbItem.getPrice().doubleValue() * num));  //小计 单价 * 数量*/
                fengzhuang(tbOrderItem,tbItem,num);
                //将创建好的明细对象,放到原来的购物车明细列表中
                orderItemList.add(tbOrderItem);  //引用传递
            }else {  //有
                //5.1.2 拿到原来的明细对象,修改num和小计
                tbOrderItem.setNum(tbOrderItem.getNum() + num); //修改数量
                tbOrderItem.setTotalFee(new BigDecimal(tbOrderItem.getPrice().doubleValue() * tbOrderItem.getNum())); //修改小计

                //若商品明细列表中该商品(tbOrderItem)的数量为0
                if(tbOrderItem.getNum().intValue() <= 0){
                    orderItemList.remove(tbOrderItem);      //就从商品明细列表中移除该商品
                }
                //若商品明细列表(orderItemList)为0,也就是商家下面没有商品了,就把这个商家从商家购物车中把该商家移除
                if(orderItemList.size() <= 0){
                    cartList.remove(cart);
                }
            }
        }
        return cartList;
    }

    //封装的方法,根据商家id查询购物车对象
    private Cart findCartBySellerId(List<Cart> cartList,String sellerId){
        for (Cart cart : cartList) {
            if(Objects.equals(cart.getSellerId(),sellerId)){
                return cart;
            }
        }
        return null;
    }

    //封装的方法,根据itemId查询购物车明细列表中的明细数据
    private TbOrderItem findOrderItemByItemId(List<TbOrderItem> orderItemList,Long itemId){
        for (TbOrderItem tbOrderItem : orderItemList) {
            if(Objects.equals(tbOrderItem.getItemId(),itemId)){
                return tbOrderItem;
            }
        }
        return null;
    }

    //封装
    private void fengzhuang(TbOrderItem tbOrderItem,TbItem tbItem,Integer num) {
        tbOrderItem.setTitle(tbItem.getTitle());//标题
        tbOrderItem.setItemId(tbItem.getId());
        tbOrderItem.setGoodsId(tbItem.getGoodsId());
        tbOrderItem.setNum(num);//数量
        tbOrderItem.setPrice(tbItem.getPrice());  //单价
        tbOrderItem.setSellerId(tbItem.getSellerId());
        tbOrderItem.setTotalFee(new BigDecimal(tbItem.getPrice().doubleValue() * num));  //小计 单价 * 数量
    }

    //将购物车数据加入到redis中
    @Override
    public void addCartListToRedis(List<Cart> cartList, String username) {
        redisTemplate.boundHashOps("cartList").put(username,cartList);
    }

    //从redis中获取购物车数据
    @Override
    public List<Cart> getCartListFromRedis(String username) {
        List<Cart> cartList = (List<Cart>)redisTemplate.boundHashOps("cartList").get(username);
        if(cartList == null){
            cartList = new ArrayList<>();
        }
        return cartList;
    }

    //合并购物车  把cookie中的购物车添加到redis中的
    @Override
    public List<Cart> merageCartList(List<Cart> cookieList, List<Cart> redisList) {
        for (Cart cart : cookieList) {
            //合并的逻辑和把数据添加到cookie购物车的逻辑一致
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            for (TbOrderItem tbOrderItem : orderItemList) {
                redisList = addCart(redisList, tbOrderItem.getItemId(), tbOrderItem.getNum());
            }
        }
        return redisList;
    }
    //增加order表
    @Override
    @Transactional
    public void submit(TbOrder tbOrder) {
        //增加order表
        tbOrderService.submit(tbOrder);
    }

    //增加orderItem表
    @Override
    public void submitOrderItem(TbOrderItem tbOrderItem) {
        tbOrderItemService.submitOrderItem(tbOrderItem);
    }
}
